

drop table if exists test_clients;
drop table if exists test_data_values;
drop table if exists test_data_values_sample;
drop table if exists test_data_values_date;
drop table if exists test_data_values_sample_date;
drop table if exists test_modules;
drop table if exists test_unique_key;
drop table if exists test_unique_key_date;

create temporary table test_data_values
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    unique_key varchar,
    value_count integer
)
;

create temporary table test_data_values_sample
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value varchar,
    type varchar,
    created_date datetime
)
;

create temporary table test_data_values_date
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    date_type varchar,
    unique_key varchar,
    len_val integer,
    type_count integer
    
)
;

create temporary table test_data_values_sample_date
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    date_type varchar,
    len_val varchar,
    value varchar,
    created_date datetime
)
;

create temporary table test_modules
(
    moduleId varchar
)
;

create temporary table test_clients
(
    client_name varchar
)
;

create temporary table test_unique_key
(
    unique_key varchar
)
;

create temporary table test_unique_key_date
(
    unique_key varchar
)
;

insert into test_clients
select client_name
from (
select distinct v.client_name
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v,
    lateral flatten(input => v.data, recursive => FALSE) f
where 1=1
    and v.created_date >= dateadd(day,$days,current_date())
    --and client_name != 'blackrock'
    and v.env = $test_env
    and (
      f.key regexp 'date.*' 
      or f.key regexp '.*DT.*'
      or f.key regexp '.*Date.*'
    )
)
sample($clientsize rows)
;

insert into test_modules
select form
from (
select distinct v.form
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v,
    lateral flatten(input => v.data, recursive => FALSE) f
where 1=1
    and v.client_name in (select client_name from test_clients)
    and v.env = $test_env
    and v.created_date >= dateadd(day,$days,current_date())
    and (
      f.key regexp 'date.*' 
      or f.key regexp '.*DT.*'
      or f.key regexp '.*Date.*'
    )
)
sample ($modulesize rows)
;

insert into test_data_values
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    typeof(f.value) as value_type,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as unique_key,
    count(id) as value_count
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name in (select client_name from test_clients)
    and v.form in (select moduleId from test_modules)
    and v.env = $test_env
    and v.created_date >= dateadd(day,$days,current_date())
group by
    v.client_name, 
    v.env, 
    v.form,
    f.key,
    typeof(f.value),
    concat(v.client_name, v.env, v.form, f.key)
order by
    v.client_name,
    v.env,
    v.form,
    f.key
;

insert into test_data_values_date
select 
    client_name,
    env,
    moduleid,
    key,
    value_type,
    date_type,
    unique_key,
    len_val,
    count(value) as type_count
from (
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as unique_key,
    typeof(f.value) as value_type,
    f.value,
    len(value) as len_val,
    (case 
     when f.value regexp '....-..-.....*' then 'YYYY-MM-DD' 
     when f.value regexp '..-..-.....*' then 'MM-DD-YYYY'
     when f.value regexp '../../.....*' then 'MM/DD/YYYY'
     when f.value regexp '[0-9\(\)]+' then 'YYYYMMDD'
     when len(f.value) = 0 then 'Missing'
     when typeof(f.value) = 'NULL_VALUE' then 'NULL'
     else 'Other' end) as date_type
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v,
    lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name in (select client_name from test_clients)
    and v.form in (select moduleId from test_modules)
    and v.env = $test_env
    and v.created_date >= dateadd(day,$days,current_date())
    and (
      f.key regexp 'date.*' 
      or f.key regexp '.*DT.*'
      or f.key regexp '.*Date.*'
    )
)
group by
    client_name,
    env,
    moduleid,
    key,
    value_type,
    date_type,
    unique_key,
    len_val
;

insert into test_unique_key
select
    unique_key
from (
select 
    unique_key, 
    count(key) as my_count
from test_data_values
where 1=1
    and value_type != 'NULL_VALUE'
group by
    unique_key
having count(key) > 1
)
;

insert into test_unique_key_date
select
    unique_key
from (
select 
    unique_key, 
    count(key) as my_count
from test_data_values_date
where 1=1
    and date_type != 'Missing'
    and date_type != 'NULL'
group by
    unique_key
having count(key) > 1
)
;

insert into test_data_values_sample
select
    client_name,
    env,
    moduleId,
    key,
    value,
    type,
    created_date
from (
select
    client_name,
    env,
    moduleId,
    key,
    value,
    created_date,
    type,
    row_number() over (
      partition by client_name, env, moduleid, key, type 
      order by created_date desc
    ) as my_row 
from (
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    f.value,
    typeof(f.value) as type,
    v.created_date,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as mkey
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and client_name in (select client_name from test_clients)
    and env = $test_env
    and moduleid in (select moduleId from test_modules)
    and key in (select distinct key from test_data_values)
    and v.created_date >= dateadd(day,$days,current_date())
)
where 1=1
    and mkey in (select unique_key from test_unique_key)
)
where 1=1
    and my_row = 1
;

insert into test_data_values_sample_date
select
    client_name,
    env,
    moduleid,
    key,
    value_type,
    date_type,
    len_val,
    value,
    created_date
from (
select 
    client_name,
    env,
    moduleid,
    key,
    value_type,
    date_type,
    unique_key,
    len_val,
    value,
    created_date,
    row_number() over (
      partition by client_name, env, moduleId, key, date_type, len_val
      order by created_date desc
    ) as my_row 
from (
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as unique_key,
    typeof(f.value) as value_type,
    f.value,
    len(value) as len_val,
    (case 
     when f.value regexp '....-..-.....*' then 'YYYY-MM-DD' 
     when f.value regexp '..-..-.....*' then 'MM-DD-YYYY'
     when f.value regexp '../../.....*' then 'MM/DD/YYYY'
     when f.value regexp '[0-9\(\)]+' then 'YYYYMMDD'
     when len(f.value) = 0 then 'Missing'
     when typeof(f.value) = 'NULL_VALUE' then 'NULL'
     else 'Other' end) as date_type,
    created_date
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v,
    lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name in (select client_name from test_clients)
    and v.form in (select moduleId from test_modules)
    and v.env = $test_env
    and v.created_date >= dateadd(day,$days,current_date())
    and (
      f.key regexp 'date.*' 
      or f.key regexp '.*DT.*'
      or f.key regexp '.*Date.*'
    )
)
where 1=1
    and unique_key in (select unique_key from test_unique_key_date)
)
where 1=1
    and my_row = 1
;

