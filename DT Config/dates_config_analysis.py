"""

Author: Ray Schaub
Date: 7/1/2021
Description: This is a new config analysis to determine submissions with differing date types

"""

import pandas as pd

"""
  note the arguments:
   - fk_client_env: the client environment number for which the test should be run.  
   This can either be a single number or a list of numbers if the user wants to
   run the test for multiple clients simultaneously.
   - snowflake_conn: the snowflake database connection object.  This is passed 
   from the main function to each individual test.
   - module_id: optional parameter to let the user run the analysis for a 
   specific module or a list of modules.
   - application_id: optional parameter to let the user run the analysis for a 
   specific application or a list of applications.
"""
  
def getMultiDateFormats(fk_client_env,snowflake_conn,module_id=None,application_id=None):
    # basic summary of what the test does
    """Within each module, identify button components for which 
    action=save and button event or trigger is set."""
    
    # define the default parameters for the SQL query
    params_dict={
      'fk_client_env':fk_client_env,
      'application_id':application_id,
      'module_id':module_id
    }

    """ 
    note the optional clauses that can be added to the query, based on the
    test arguments:
    - fk_client_env_field: a portion of the main select that includes the 
    fk_client_env if the test is being run for multiple clients rather than a
    single client
    - app_fields: a portion of the main select that includes the application 
    id and application name.  These will be included unless the user only runs
    the analysis on a specific module or list of modules.
    - app_join: a join on the applications_modules_v0 table to restrict the results
    to only include modules that are either part of the specific applications or
    part of at least one application if no applications are specified.
    - app_clause: a clause to restrict the results to only include modules that
    are included in the specified applications
    - module_clause: a clause to restrict the results to only include the specified
    modules.
    - order_by_clause: the fields to use in the order by clause for the query
    """
        
    app_clause=''
    module_clause=''    
    module_clause2=''
    
    if not isinstance(fk_client_env,list):
        fk_client_env=[fk_client_env]
    
    """
    if the test is not being run for a specific module, restrict the the results
    to only include modules that are either part of the specified application
    or part of at least one application if no applications are specified.
    """
    if module_id is not None:
        module_clause2="and module_id != 'None'"
        if isinstance(module_id,list):
            module_clause = 'and s.some_id in (%(module_id)s)'
        else: 
            module_clause = 'and s.some_id = %(module_id)s'
        
    if application_id is not None:
        if isinstance(application_id,list):
            app_clause="and av.id in (%(application_id)s)"
        else:
            app_clause="and av.id = %(application_id)s"

    """
    if the test is being run on specific modules , 
    restrict the results to just those modules 
    """
    
    query = open('/home/jovyan/personal/Ray/DT Config/dates_config_analysis.sql').read()
    
    query = query.format(app_clause,module_clause,app_clause,module_clause2)
 
    results=pd.read_sql_query(query,con=snowflake_conn,params=params_dict,parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'})
    return results
