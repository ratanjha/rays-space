
"""

This is my first attempt at connecting to Snowflake. I will use the Data Types config analysis 
Snowflake query as test query. I prod, this will be formatted like all config analysis, as 
indicated by the confluence page: 
https://unqork-wiki.atlassian.net/wiki/spaces/DATA/pages/1810759762/Create+a+New+Config+Analysis+Test

I don't need write capabilities in this script. Therefore, I will just save this info for now:
conn_ferm=snowflake.connector.connect(
    account=snowflake_host,
    warehouse=snowflake_wh,
    database=snowflake_db,
    schema='FERMENTATION',
    user=snowflake_username,
    password=snowflake_password
)

Adding variables to a query, as well as running a basic command (as opposed to importing direct to pandas):
cs=conn.cursor()
params_dict={'client':client,'env':env,'application':application,'module':module}
query="select pk_client_env from lookup_client_env_v0 where client_name=%(client)s and env=%(env)s"
cs.execute(query,params_dict)

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date
from datetime import timedelta
import time
import json
import getpass
import itertools
import re

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

client = 'hcin-crr'
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
env = input()

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

params_dict={'env':env,'client_num':1,'module_num':400,'days':-14}
cs=conn.cursor()

query = """
set (days, clientsize, modulesize, test_env) = (%(days)s, %(client_num)s, %(module_num)s, %(env)s);
"""

cs.execute(query,params_dict)

query = open('/home/jovyan/personal/Ray/dt_config_query.sql').read().split(';')

for i in range(len(query)-1):
    cs.execute(query[i] + ';',params_dict)

query="""
select *
from test_data_values
where 1=1
    and unique_key in (select unique_key from test_unique_key)
;
"""

types_data=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

types_data.columns = [x.lower() for x in types_data.columns]

query="""
select *
from test_data_values_sample
where 1=1
;
"""

types_data_sample=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

types_data_sample.columns = [x.lower() for x in types_data_sample.columns]

query = """
select *
from test_data_values_date
where 1=1
    and unique_key in (select unique_key from test_unique_key_date)
;
"""

date_data=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

date_data.columns = [x.lower() for x in date_data.columns]

query = """
select *
from test_data_values_sample_date
where 1=1
;
"""

date_data_sample=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

date_data_sample.columns = [x.lower() for x in date_data_sample.columns]

client_vars = ['client_name','env','moduleid','key','value_type']
my_types = list(types_data.value_type.unique())

types_data2 = types_data.groupby(client_vars).size().reset_index().rename(columns={0:'_size'})
types_data2 = types_data2.pivot_table(index=client_vars[:-1],columns=client_vars[-1],values='_size').fillna(0).reset_index()
types_data2['TOTAL']= types_data2[my_types].sum(axis=1)

types_data2['category'] = None
for i in range(len(types_data2)):
    types_data2.iloc[i,list(types_data2.columns).index('category')] = ','.join([x[0] for x in 
        types_data2[my_types].iloc[i,:].to_dict().items() if x[1] > 0])

date_data['date_type_len'] = date_data.date_type + '-' + date_data.len_val.astype('str')
client_vars_date = ['client_name','env','moduleid','key','date_type_len']
my_types_date = list(date_data.date_type_len.unique())

date_data2 = date_data.groupby(client_vars_date).size().reset_index().rename(columns={0:'_size'})
date_data2 = date_data2.pivot_table(index=client_vars_date[:-1],columns=client_vars_date[-1],values='_size').fillna(0).reset_index()
date_data2['TOTAL']= date_data2[my_types_date].sum(axis=1)

date_data2['category'] = None
for i in range(len(date_data2)):
    date_data2.iloc[i,list(date_data2.columns).index('category')] = ','.join([x[0] for x in 
        date_data2[my_types_date].iloc[i,:].to_dict().items() if x[1] > 0])

importance_table = pd.DataFrame(list(itertools.product(my_types,my_types)), columns=['type1','type2'])
importance_table = importance_table.loc[(importance_table.type1!='NULL_VALUE') & (importance_table.type2!='NULL_VALUE')]
importance_table = importance_table.loc[(importance_table.type1!=importance_table.type2)].reset_index().drop('index',axis=1)
importance_dict = dict(zip(['VARCHAR', 'ARRAY', 'INTEGER','DECIMAL', 'OBJECT' ],range(1,6)))
importance_table

def importance_weight(x,y):
    if x == 'INTEGER':
        if y == 'DECIMAL':
            return(1)
        else: return(3)
    elif x == 'DECIMAL':
        if y == 'INTEGER':
            return(1)
        else: return(3)
    elif x == 'VARCHAR':
        if y in ('DECIMAL','INTEGER'):
            return(3)
        else: return(2)
    else: 
        if y in ('DECIMAL','INTEGER'):
            return(3)
        elif y == 'VARCHAR':
            return(2)
        else: return(1)
importance_table['importance'] = importance_table.apply(lambda x: importance_weight(x.type1,x.type2),axis=1)

types_data2['importance'] = types_data2.apply(lambda x: importance_table.loc[np.logical_not(importance_table.apply(lambda s: re.search(s.type1,x.category),axis=1).isna()) & 
                np.logical_not(importance_table.apply(lambda s: re.search(s.type2,x.category),axis=1).isna())].reset_index().importance.max(), axis=1)

client = '-'.join(list(types_data2.client_name.unique()))

types_data_sample.loc[np.logical_not(types_data_sample.value.isna()),'value'] = types_data_sample.loc[np.logical_not(types_data_sample.value.isna())].value.apply(lambda x: x[0:5000])

#create a google sheet.
#derive the name from the client, environment, and time stamp in UTC.
file_name='DT Config Analysis {} {} {} UTC'.format(client,env,time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#to authorize a Google Sheets API to write files to a user's Google Drive, follow the steps here:
#https://pygsheets.readthedocs.io/en/stable/authorization.html
#I would recommend that you follow the steps for OAuth Credentials rather than a Service account.
#To work correctly, the command below requires a json file with the OAuth authentication token to exist in the current working directory

#alternatively, these commands could be used to authenticate the file from a service account environment variable
#gc=pygsheets.authorize(service_account_env_var='SVC_AUTH')

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

#spreadsheet_body=create_spreadsheet_json(results2)

def gsheets(credentials,file_name,folder):
    gc=Client(credentials,retries=50)
    output_sheets=gc.create(file_name,folder=folder)

    #spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
    spreadsheet = gc.open(file_name)
    
    try: 
        spreadsheet.add_worksheet('Count_by_Data_Type', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.add_worksheet('Sample_Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.add_worksheet('Date_Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.add_worksheet('Date_Sample_Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')

    wks1 = spreadsheet.worksheet_by_title('Count_by_Data_Type')
    wks1.set_dataframe(types_data2, start=(1,1))

    wks2 = spreadsheet.worksheet_by_title('Sample_Data')
    wks2.set_dataframe(types_data_sample, start=(1,1))

    wks3 = spreadsheet.worksheet_by_title('Date_Data')
    wks3.set_dataframe(date_data2, start=(1,1))

    wks4 = spreadsheet.worksheet_by_title('Date_Sample_Data')
    wks4.set_dataframe(date_data_sample, start=(1,1))

#print(env)
conn.close()