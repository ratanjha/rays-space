
"""

Author: Ray Schaub
Date: 6/9/2021
Description: Used to find Module submissions with differing date types

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date
from datetime import timedelta
import time
import json
import getpass
import itertools
import re

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

print('client:')
client = input()
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
print('env:')
env = input()

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")


cs=conn.cursor()

if client != '':
    params_dict={'client':client,'env':env,'client_num':1,'days':-14}
    query = """
    set (client, days, clientsize, test_env) = (%(client)s, %(days)s, %(client_num)s, %(env)s);
    """
else :
    params_dict={'env':env,'client_num':1,'days':-14}
    query = """
    set (days, clientsize, test_env) = (%(days)s, %(client_num)s, %(env)s);
    """
    
cs.execute(query,params_dict)

query = open('/home/jovyan/personal/Ray/DT Config/date_types_config_analysis.sql').read()
if client != '': 
    query = query.replace("--and client_name = ''","and client_name = $client")
    
query = query.split(';')

for i in range(len(query)-1):
    cs.execute(query[i] + ';',params_dict)

query="""
select 
    v.client_name,
    v.env,
    v.moduleId,
    v.key,
    v.date_type,
    v.value_type,
    v.len_val,
    v.type_count,
    s.submissionId,
    s.value as submission_sample,
    s.created_date
from 
    test_data_values_date v,
    test_data_values_sample_date s
where 1=1
    and v.client_name = s.client_name
    and v.env = s.env
    and v.moduleId = s.moduleId
    and v.key = s.key
    and v.value_type=s.value_type
    and v.date_type = s.date_type
    and v.len_val = s.len_val
order by
    client_name,
    env,
    moduleid,
    key
;
"""

date_data=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

date_data.columns = [x.lower() for x in date_data.columns]

date_data['date_type_len'] = date_data.date_type + '-' + date_data.len_val.astype('str') + '-' + date_data.value_type
client_vars_date = ['client_name','env','moduleid','key']
my_types_date = list(date_data.date_type_len.unique())

date_data['ratio'] = date_data.type_count/date_data.groupby(client_vars_date).type_count.transform('sum')
date_data['category'] = date_data.groupby(client_vars_date).date_type_len.transform(lambda x: ','.join(x))

date_data = date_data.loc[date_data.ratio < 1]

client2 = '-'.join(list(date_data.client_name.unique()))

date_data.loc[np.logical_not(date_data['submission_sample'].isna()),'submission_sample'] = date_data.loc[np.logical_not(date_data['submission_sample'].isna())].submission_sample.apply(lambda x: x[0:5000])
#types_data = types_data.sort_values('importance',ascending=False)

#create a google sheet.
#derive the name from the client, environment, and time stamp in UTC.
file_name='Date Config Analysis {} {} {} UTC'.format(client2,env,time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#to authorize a Google Sheets API to write files to a user's Google Drive, follow the steps here:
#https://pygsheets.readthedocs.io/en/stable/authorization.html
#I would recommend that you follow the steps for OAuth Credentials rather than a Service account.
#To work correctly, the command below requires a json file with the OAuth authentication token to exist in the current working directory

#alternatively, these commands could be used to authenticate the file from a service account environment variable
#gc=pygsheets.authorize(service_account_env_var='SVC_AUTH')

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

#spreadsheet_body=create_spreadsheet_json(results2)

def gsheets(credentials,file_name,folder):
    gc=Client(credentials,retries=50)
    output_sheets=gc.create(file_name,folder=folder)

    #spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
    spreadsheet = gc.open(file_name)
    
    try: 
        spreadsheet.add_worksheet('Count_by_Date_Type', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')

    wks1 = spreadsheet.worksheet_by_title('Count_by_Date_Type')
    wks1.set_dataframe(date_data, start=(1,1))

#print(env)
conn.close()