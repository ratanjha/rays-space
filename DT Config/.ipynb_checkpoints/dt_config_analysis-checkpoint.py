
"""

This is my first attempt at connecting to Snowflake. I will use the Data Types config analysis 
Snowflake query as test query. I prod, this will be formatted like all config analysis, as 
indicated by the confluence page: 
https://unqork-wiki.atlassian.net/wiki/spaces/DATA/pages/1810759762/Create+a+New+Config+Analysis+Test

I don't need write capabilities in this script. Therefore, I will just save this info for now:
conn_ferm=snowflake.connector.connect(
    account=snowflake_host,
    warehouse=snowflake_wh,
    database=snowflake_db,
    schema='FERMENTATION',
    user=snowflake_username,
    password=snowflake_password
)

Adding variables to a query, as well as running a basic command (as opposed to importing direct to pandas):
cs=conn.cursor()
params_dict={'client':client,'env':env,'application':application,'module':module}
query="select pk_client_env from lookup_client_env_v0 where client_name=%(client)s and env=%(env)s"
cs.execute(query,params_dict)

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date
from datetime import timedelta
import time
import json
import getpass

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
snowflake_password=getpass.getpass(prompt='', stream=None)

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

client = 'hcin-crr'
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
env = input()

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

params_dict={'client':client,'env':env,'lim_num':200,'days':-14}
cs=conn.cursor()

query = """
drop table if exists test_data_values;
"""

cs.execute(query,params_dict)


query = """
drop table if exists test_data_values_date;
"""

cs.execute(query,params_dict)

query = """
drop table if exists test_modules;
"""

cs.execute(query,params_dict)

query = """
create temporary table test_data_values
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    unique_key varchar,
    value_count integer
)
;
"""

cs.execute(query,params_dict)

query = """
create temporary table test_data_values_date
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    value varchar,
    len_val integer,
    date_type varchar
)
;
"""

cs.execute(query,params_dict)

query="""
create temporary table test_modules
(
    moduleId varchar
)
;
"""

cs.execute(query,params_dict)

query="""
insert into test_modules
select distinct v.form
from Dev.HARVESTING.SUBMISSIONS_V0 v
where 1=1
    and v.client_name = %(client)s
    and v.env = %(env)s
;
"""

cs.execute(query,params_dict)

query = """
insert into test_data_values
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    typeof(f.value) as value_type,
    concat(v.client_name, v.env, v.form, f.key) as unique_key,
    count(id) as value_count
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name = %(client)s
    and v.form in (select moduleId from test_modules limit %(lim_num)s)
    and v.env = %(env)s
    and v.created_date >= dateadd(day,%(days)s,current_date())
group by
    v.client_name, 
    v.env, 
    v.form,
    f.key,
    typeof(f.value),
    concat(v.client_name, v.env, v.form, f.key)
order by
    v.client_name,
    v.env,
    v.form,
    f.key
;
"""

cs.execute(query,params_dict)

query = """
insert into test_data_values_date
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    typeof(f.value) as value_type,
    f.value,
    len(value) as len_val,
    (case when value regexp '____-__-__.*' then 'YYYY-MM-DD' else 'Other' end) as date_type
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v,
    lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name = %(client)s
    and v.form in (select moduleId from test_modules limit %(lim_num)s)
    and v.env = %(env)s
    and v.created_date >= dateadd(day,%(days)s,current_date())
    and (lower(f.key) regexp '.*date.*' or f.key regexp '.*DT.*')
    and lower(f.key) not regexp '.*validate.*'
order by
    v.client_name,
    v.env,
    v.form,
    f.key
;
"""

cs.execute(query,params_dict)

query="""
select 
    client_name,
    env,
    moduleid,
    key,
    value_type,
    value_count
from test_data_values
where 1=1
    and unique_key in (
        select unique_key
        from (
            select 
                unique_key, 
                count(moduleid) as _count
            from test_data_values
            where 1=1
                and value_type != 'NULL_VALUE'
            group by unique_key
            having count(*) > 1
          )
     )
order by
    client_name,
    env,
    moduleId,
    key,
    value_type
;
"""

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

my_types = list(results.value_type.unique())

results2 = results.pivot_table(index=['client_name','moduleid','key'],
        columns='value_type',values='value_count').fillna(0).reset_index()

results2['TOTAL']= results2[my_types].sum(axis=1)

results2['category'] = None
for i in range(len(results2)):
    results2.iloc[i,list(results2.columns).index('category')] = ','.join([x[0] for x in 
        results2[my_types].iloc[i,:].to_dict().items() if x[1] > 0])

print(results2)

categories = list(results2.category.unique())
print(categories)

r_category = random.choice(categories)
print(r_category)
#moduleId = list(results2.loc[results2.category==r_category].moduleid)
moduleId = list(results2.moduleid)
#Key = list(results2.loc[results2.category==r_category].key)
Key = list(results2.key)
#moduleidkey = list(results2.loc[results2.category==r_category,['moduleid','key']].agg('-'.join,axis=1))
moduleidkey = list(results2[['moduleid','key']].agg('-'.join,axis=1))

params_dict={'client':client,'env':env,'lim_num':2000,'days':-14,'mkey':moduleidkey,'moduleid':moduleId,
    'key':Key}

query = """
select
    client_name,
    env,
    moduleId,
    key,
    value,
    type,
    created_date
from (
select
    client_name,
    env,
    moduleId,
    key,
    value,
    created_date,
    type,
    row_number() over (
      partition by moduleId, key, type 
      order by created_date desc
    ) as my_row 
from (
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    f.value,
    typeof(f.value) as type,
    v.created_date,
    concat(v.form,'-',f.key) as mkey
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and client_name = %(client)s
    and env = %(env)s
    and moduleid in (%(moduleid)s)
    and key in (%(key)s)
    and v.created_date >= dateadd(day,%(days)s,current_date())
)
where 1=1
    and mkey in (%(mkey)s)
)
where 1=1
    and my_row = 1
order by
    client_name,
    env,
    moduleid,
    key,
    type,
    created_date
limit %(lim_num)s
;
"""

sample_data=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

sample_data.columns = [x.lower() for x in sample_data.columns]
print(sample_data)

query = """
select
    client_name,
    env,
    moduleID,
    key,
    len_val,
    date_type,
    count(value) as type_count
from (
select 
    client_name,
    env,
    moduleid,
    key,
    value,
    len_val,
    date_type
from test_data_values_date
where 1=1
)
group by
    client_name,
    env,
    moduleID,
    key,
    len_val,
    date_type
;
"""

date_data=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

date_data.columns = [x.lower() for x in date_data.columns]
print(date_data)

date_data['type'] = date_data.len_val.astype('str') + '_' + date_data.date_type

my_date_types = list(date_data.type.unique())

"""
date_data = date_data.pivot_table(index=['client_name','moduleid','key'],
        columns='type',values='type_count').fillna(0).reset_index()

date_data['TOTAL']= date_data[my_date_types].sum(axis=1)

date_data['category'] = None
for i in range(len(date_data)):
    date_data.iloc[i,list(date_data.columns).index('category')] = ','.join([x[0] for x in 
        date_data[my_date_types].iloc[i,:].to_dict().items() if x[1] > 0])

#create a google sheet.
#derive the name from the client, environment, and time stamp in UTC.
file_name='DT Config Analysis {} {} {} UTC'.format(client,env,time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#to authorize a Google Sheets API to write files to a user's Google Drive, follow the steps here:
#https://pygsheets.readthedocs.io/en/stable/authorization.html
#I would recommend that you follow the steps for OAuth Credentials rather than a Service account.
#To work correctly, the command below requires a json file with the OAuth authentication token to exist in the current working directory

#alternatively, these commands could be used to authenticate the file from a service account environment variable
#gc=pygsheets.authorize(service_account_env_var='SVC_AUTH')

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

gc=Client(credentials,retries=50)
#spreadsheet_body=create_spreadsheet_json(results2)
output_sheets=gc.create(file_name,folder=folder)

#spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
spreadsheet = gc.open(file_name)

spreadsheet.add_worksheet('Count_by_Data_Type', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
spreadsheet.add_worksheet('Sample_Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
spreadsheet.add_worksheet('Date_Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
spreadsheet.del_worksheet(spreadsheet.sheet1)

wks1 = spreadsheet.worksheet_by_title('Count_by_Data_Type')
wks1.set_dataframe(results2, start=(1,1))

wks2 = spreadsheet.worksheet_by_title('Sample_Data')
wks2.set_dataframe(sample_data, start=(1,1))

wks3 = spreadsheet.worksheet_by_title('Date_Data')
wks3.set_dataframe(date_data, start=(1,1))
"""
#print(env)
conn.close()