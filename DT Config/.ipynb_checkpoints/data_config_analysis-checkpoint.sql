
with subs as (
select 
    fk_client_env,
    id as submission_id,
    form as some_id,
    created_date,
    data
from dev.harvesting.submissions_v0
where 1=1
    and created_date >= dateadd(day, -7, current_date())
    and fk_client_env in (%(fk_client_env)s)
),
mods as (
select
    amv.application_id,
    av.name as application_name,
    mv.id as module_id,
    mv.name as module_name,
    s.*
from 
    dev.harvesting.applications_modules_v0 amv,
    dev.harvesting.modules_v0 mv,
    dev.harvesting.applications_v0 av,
    subs s
where 1=1
    and s.fk_client_env=mv.fk_client_env
    and s.fk_client_env=amv.fk_client_env
    and s.fk_client_env=av.fk_client_env
    and s.some_id=mv.id
    and s.some_id=amv.module_id
    and amv.application_id=av.id
    {}
    {}
),
apps as (
select
    av.id as application_id,
    av.name as application_name,
    'None' as module_id,
    'None' as module_name,
    s.*
from 
    dev.harvesting.applications_v0 av,
    subs s
where 1=1
    and s.fk_client_env=av.fk_client_env
    and s.some_id=av.id
    and s.some_id not in (
    select some_id from mods)
    {}
),
total as (
select *
from mods
where 1=1
union all
select *
from apps
where 1=1
),
a as (
select 
    v.fk_client_env,
    v.application_id,
    v.application_name,
    v.module_id,
    v.module_name,
    v.created_date,
    regexp_replace(f.path, '\[[0-9]+\]','\[\]') as key,
    typeof(f.value) as  value_type,
    f.value,
    row_number() over (partition by v.fk_client_env, v.application_id, v.module_id, f.key order by v.created_date desc) as my_row
from 
    total v,
    lateral flatten(input => v.data, recursive => True) f
where 1=1
    and v.created_date >= dateadd(day,-7,current_date())
),
a2 as (
select
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    created_date,
    key,
    value_type,
    value,
    row_number() over (partition by fk_client_env, application_id, module_id, key order by created_date desc) as my_row
from a
where 1=1
),
b as (
select 
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key,
    value_type,
    value,
    len(value) as len_val,
    my_row 
from a2
where 1=1
    and value_type != 'ARRAY'
),
e as (
select 
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key,
    value_type,
    min(my_row) as my_row,
    count(value) as type_count
from b
where 1=1
group by
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key,
    value_type
),
f as (
select
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key,
    value_type,
    my_row,
    type_count,
    count(fk_client_env) over (partition by fk_client_env, application_id, module_id, key) as diff_count
from e
where 1=1
    and value_type != 'NULL_VALUE'
),
g as (
select *
from f
where 1=1
    and diff_count > 1
),
h as (
select 
    g.fk_client_env,
    g.application_id,
    g.application_name,
    g.module_id,
    g.module_name,
    g.key,
    g.value_type,
    g.type_count,
    a.value as sample_value,
    a.created_date as sample_datetime
from g, a2 a
where 1=1
    and a.fk_client_env = g.fk_client_env
    and a.application_id = g.application_id
    and a.module_id = g.module_id
    and a.key = g.key
    and a.my_row = g.my_row
order by
    g.fk_client_env,
    g.application_id,
    g.application_name,
    g.module_id,
    g.module_name,
    g.key,
    g.value_type
), 
k as (
select
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key as field_name,
    array_agg(object_construct('data_type',value_type, 'count', type_count,
              'sample', sample_value, ' sample_date', sample_datetime)) within group (order by key asc) as data
from h
where 1=1
group by
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    key
)
select 
    fk_client_env,
    application_id,
    application_name,
    module_id,
    module_name,
    field_name,
    regexp_replace(regexp_replace(regexp_replace(to_json(data), '\\\]|\\\[','' ),'\\{{','('),'\\}}',')') as data
from k
where 1=1
;