

drop table if exists test_clients;
drop table if exists test_data_values;
drop table if exists test_data_values_sample;
drop table if exists test_unique_key;

create temporary table test_data_values
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    key varchar,
    value_type varchar,
    unique_key varchar,
    value_count integer
)
;

create temporary table test_data_values_sample
(
    client_name varchar,
    env varchar,
    moduleId varchar,
    submissionId varchar,
    key varchar,
    value varchar,
    type varchar,
    created_date datetime
)
;

create temporary table test_clients
(
    client_name varchar
)
;

create temporary table test_unique_key
(
    unique_key varchar
)
;

insert into test_clients
select client_name
from (
select distinct v.client_name
from 
    Dev.HARVESTING.SUBMISSIONS_V0 v
where 1=1
    and v.created_date >= dateadd(day,$days,current_date())
    --and client_name = ''
    and v.env = $test_env
)
sample($clientsize rows)
;

insert into test_data_values
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    typeof(f.value) as value_type,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as unique_key,
    count(id) as value_count
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and v.client_name in (select client_name from test_clients)
    and v.env = $test_env
    and v.created_date >= dateadd(day,$days,current_date())
group by
    v.client_name, 
    v.env, 
    v.form,
    f.key,
    typeof(f.value),
    concat(v.client_name, v.env, v.form, f.key)
order by
    v.client_name,
    v.env,
    v.form,
    f.key
;

insert into test_unique_key
select
    unique_key
from (
select 
    unique_key, 
    count(key) as my_count
from test_data_values
where 1=1
    and value_type != 'NULL_VALUE'
group by
    unique_key
having count(key) > 1
)
;

insert into test_data_values_sample
select
    client_name,
    env,
    moduleId,
    submissionId,
    key,
    value,
    type,
    created_date
from (
select
    client_name,
    env,
    moduleId,
    submissionId,
    key,
    value,
    created_date,
    type,
    row_number() over (
      partition by client_name, env, moduleid, key, type 
      order by created_date desc
    ) as my_row 
from (
select 
    v.client_name, 
    v.env, 
    v.form as moduleID,
    f.key,
    f.value,
    v.ID as submissionId,
    typeof(f.value) as type,
    v.created_date,
    concat(v.client_name, '-', v.env, '-', v.form, '-', f.key) as mkey
from Dev.HARVESTING.SUBMISSIONS_V0 v,
lateral flatten(input => v.data, recursive => TRUE) f
where 1=1
    and client_name in (select client_name from test_clients)
    and env = $test_env
    and key in (select distinct key from test_data_values)
    and v.created_date >= dateadd(day,$days,current_date())
    and f.value != ''
    and f.value is not NULL
)
where 1=1
    and mkey in (select unique_key from test_unique_key)
)
where 1=1
    and my_row = 1
;

