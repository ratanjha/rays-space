with a as (
select 
    metadata_client as client,
    metadata_env as env,
    regexp_replace(regexp_substr(request_uri,'.*moduleId=[0-9a-z]*'),'.*moduleId=','') as moduleId
from dev.harvesting.AWS_ELB_LOGS_V0
where 1=1
    and time >= dateadd(day,-2,current_timestamp())
    and request_uri regexp '.*&moduleId.*'
    and request_processing_time >= 0
    and target_processing_time > 1
), b as (
select
    client,
    env,
    moduleId,
    count(client) as get_count
from a
where 1=1
group by
    client,
    env,
    moduleId
), c as (
select 
    client,
    env,
    moduleId,
    get_count,
    nth_value(get_count,1) over (order by get_count desc) as get_count_n
from b
where 1=1
), c2 as (
select *
from c
where 1=1
    and get_count=get_count_n
),d as (
select 
    v.metadata_client as client,
    v.metadata_env as env,
    v.elb,
    v.elb_status_code,
    v.received_bytes,
    v.request_host,
    v.request_method,
    v.request_port,
    v.request_processing_time,
    v.request_protocol,
    v.request_uri,
    v.response_processing_time,
    v.sent_bytes,
    v.ssl_protocol,
    v.target_name,
    v.target_port,
    v.target_processing_time,
    v.target_status_code,
    v.time,
    v.metadata_date as date,
    regexp_replace(regexp_substr(v.request_uri,'.*moduleId=[0-9a-z]*'),'.*moduleId=','') as moduleId,
    c.get_count
from 
    dev.harvesting.AWS_ELB_LOGS_V0 v,
    c2 c
where 1=1
    and v.metadata_client = c.client
    and v.metadata_env = c.env
    and v.request_uri regexp '.*&moduleId.*'
    and regexp_replace(regexp_substr(v.request_uri,'.*moduleId=[0-9a-z]*'),'.*moduleId=','')=c.moduleId
    and v.time >= dateadd(year,-2, current_date())
), e as (
select 
    client_name,
    env,
    form as moduleId,
    id as submissionId,
    created_date as submissionCreatedDate
from dev.harvesting.submissions_v0
where 1=1
    and form in (select moduleId from c)
)
select 
    a.client,
    a.env,
    a.elb,
    a.elb_status_code,
    a.received_bytes,
    a.request_host,
    a.request_method,
    a.request_port,
    a.request_processing_time,
    a.request_protocol,
    a.request_uri,
    a.response_processing_time,
    a.sent_bytes,
    a.ssl_protocol,
    a.target_name,
    a.target_port,
    a.target_processing_time,
    a.target_status_code,
    a.time,
    a.date,
    a.moduleId,
    count(b.submissionId) as sub_count
from d a, e b
where 1=1
    and a.client=b.client_name
    and a.env=b.env
    and a.moduleId=b.moduleId
    and a.time >= b.submissionCreatedDate
    and a.target_status_code = '200'
    --and a.date >= '2021-07-20'
group by
    a.client,
    a.env,
    a.elb,
    a.elb_status_code,
    a.received_bytes,
    a.request_host,
    a.request_method,
    a.request_port,
    a.request_processing_time,
    a.request_protocol,
    a.request_uri,
    a.response_processing_time,
    a.sent_bytes,
    a.ssl_protocol,
    a.target_name,
    a.target_port,
    a.target_processing_time,
    a.target_status_code,
    a.time,
    a.date,
    a.moduleId
order by
    a.client,
    a.env,
    a.moduleId,
    a.time
;