
"""

Author: Ray Schaub
Date: 10/21/2021
Description: Identify different memory categories and then detect "issue" request types

Packages not installed globally:
pip install datadog
pip install datadog-api-client

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import datadog
from datadog import initialize
from datadog import api
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
from pprint import pprint
from sklearn.ensemble import IsolationForest
from scipy import stats

#########################################################
##                                                     ##
##     Set up Env Variables & set Pandas options       ##
##                                                     ##
#########################################################

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())
pd.set_option('display.float_format', lambda x: '%.6f' % x)

#########################################################
##                                                     ##
##         Set up Snowflake Connection Params          ##
##                                                     ##
#########################################################

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()
print('Start Date:')
startDate = input()
print('End Date:')
endDate = input()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh='compute_wh'

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

#########################################################
##                                                     ##
##            Create Snowflake query list              ##
##                                                     ##
#########################################################
    
queries = open('/home/jovyan/personal/Ray/rays-space/Access Logs/nodejs_mem2.sql').read()

queries = [x + ';' for x in queries.split(';')]

#########################################################
##                                                     ##
##         Set up Datadog Connection Params            ##
##                                                     ##
#########################################################
    
initialize(
    api_key=os.environ.get('DD_API_KEY'),
    app_key=os.environ.get('DD_APP_KEY'),
    statsd_host="127.0.0.1",
    statsd_port=8125
)

#########################################################
##                                                     ##
##               Query the Datadog API                 ##
##                                                     ##
#########################################################

days = 30
test = api.Metric.query(start=int(time.time()) - 3600*24*(days), end=int(time.time()),
        query='avg:system.mem.pct_usable{uq-environmentname:*} by {uq-environmentname}')

test=test['series']
env_list = [x['tag_set'][0] for x in test]
df = pd.DataFrame()

for e in env_list:
    list1 = [x['pointlist'] for x in test if x['tag_set'][0]==e]
    df1 = pd.DataFrame(list1).transpose().rename(columns={0:'pointlist'})
    df1['time'] = pd.to_datetime(df1.pointlist.apply(lambda x: x[0]*1000000))
    df1['freeMem'] = df1.pointlist.apply(lambda x: x[1])
    df1['client_env'] = re.sub('uq-environmentname:','',e)
    df = df.append(df1)
    
df['client'] = df.client_env
uat_list = ['uat','qa-uat','staging','dev-uat','sandbox-uat','production','preprod-production']
for a in uat_list:
    df['client'] = np.where(df.client_env.str.contains(a),df.client_env.apply(lambda x: re.sub('-'+a,'',x)),df.client)
df['env'] = df.apply(lambda x: re.sub(x.client+'-','',x.client_env),axis=1)
   
df2 = df.groupby(['client','env']).freeMem.describe().reset_index()

#########################################################
##                                                     ##
##         Categorize Envs by Memory "Types"           ##
##                                                     ##
#########################################################

highStdList = list(df2.loc[df2['std'] >= (df2['std'].mean()+df2['std'].std()*1.5)].apply(lambda x: x.client + '_' + x.env, axis=1))
lowMemList = list(df2.loc[df2['min'] <= .3].apply(lambda x: x.client + '_' + x.env, axis=1))
lowStdList = list(df2.loc[df2['std'] <= (df2['std'].mean()-df2['std'].std())*1].apply(lambda x: x.client + '_' + x.env, axis=1))
highMemList = list(df2.loc[df2['min'] >= .55].apply(lambda x: x.client + '_' + x.env, axis=1))
df['highStd'] = np.where(df.apply(lambda x: x.client+'_'+x.env,axis=1).isin(highStdList),1,0)
df['lowMem'] = np.where(df.apply(lambda x: x.client+'_'+x.env,axis=1).isin(lowMemList),1,0)
df['lowStd'] = np.where(df.apply(lambda x: x.client+'_'+x.env,axis=1).isin(lowStdList),1,0)
df['highMem'] = np.where(df.apply(lambda x: x.client+'_'+x.env,axis=1).isin(highMemList),1,0)

final = df.loc[(df['lowMem']==1) | (df['highMem']==1)].copy()

clients = list(final.client.unique())
envs = list(final.env.unique())
client_env = list(final.apply(lambda x: x.client + '_' + x.env, axis=1).unique())

#########################################################
##                                                     ##
##             Query the Snowflake API                 ##
##                                                     ##
#########################################################

cs=conn.cursor()

params_dict={'env':envs,'client':clients,'startDate':startDate,'endDate':endDate,'client-env':client_env}

query = queries[0]

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

#########################################################
##                                                     ##
##   Find Request that differ by Significane Testing   ##
##                                                     ##
#########################################################

results['category'] = np.where(results.apply(lambda x: x.client+'_'+x.env,axis=1).isin(lowMemList),'lowMem','highMem')
results2 = results.groupby(['date','request_type','category']).request_count.sum().reset_index()
results2['request_count_total'] = results.groupby(['date','category']).request_count.transform('sum')
results2['request_count_percent'] = results2.request_count/results2.request_count_total
results2 = pd.pivot_table(results2,values='request_count_percent',index=['date','request_type'],columns='category',aggfunc='mean').reset_index().fillna(0)

req_list = list(results2.request_type.unique())
df_sigtest = pd.DataFrame(columns=['request_type','p_value'])

for x in req_list:
    welch = stats.ttest_ind(results2.loc[(results2.request_type==x)].lowMem, results2.loc[(results2.request_type==x)].highMem,equal_var=False)
    df_sigtest = df_sigtest.append(pd.DataFrame([x, welch.pvalue]).transpose().rename(columns={0:'request_type',1:'p_value'}))
    
df_sigtest = df_sigtest.merge(results2.groupby(['request_type']).agg({'lowMem':'mean','highMem':'mean'}).reset_index())
df_sigtest['ratio'] = df_sigtest.lowMem/df_sigtest.highMem
df_sigtest.sort_values('p_value',inplace=True)

#########################################################
##                                                     ##
##     Percent of Envs that have Request by Type       ##
##                                                     ##
#########################################################

results3 = results.groupby(['client','env','request_type']).request_count.sum().reset_index()
results3['request_count'] = 1
results3['client_env'] = results3.client + '_' + results3.env
results3 = pd.pivot_table(results3,values='request_count',index='request_type',columns='client_env',aggfunc='sum').reset_index().fillna(0)
results3['mean_env'] = results3.apply(lambda x: x[2:].mean(), axis=1)
results3[['request_type','mean_env']]

#########################################################
##                                                     ##
##        Identify Key Requests That Stand out         ##
##                                                     ##
#########################################################

dfOnlyInLow = df_sigtest.loc[df_sigtest.ratio == np.inf]
dfSignificantLow = df_sigtest.loc[(df_sigtest.ratio>1)&(df_sigtest.p_value<.05)]

#########################################################
##                                                     ##
##            Setup Variables for Query                ##
##                                                     ##
#########################################################

final2 = df.loc[(df['lowMem']==1)].copy()

clients2 = list(final2.client.unique())
envs2 = list(final2.env.unique())
client_env2 = list(final2.apply(lambda x: x.client + '_' + x.env, axis=1).unique())
req_list2 = list(dfSignificantLow.request_type) + list(dfOnlyInLow.request_type)

#########################################################
##                                                     ##
##             Query the Snowflake API 2               ##
##                                                     ##
#########################################################

query = queries[1]
req_dict = {}

for x in req_list2:
    params_dict={'env':envs2,'client':clients2,'startDate':startDate,'endDate':endDate,'client-env':client_env2,'request_types':x}
    
    req_dict[x]=pd.read_sql_query(
        query,
        con=conn,
        params=params_dict,
        #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
    )

    req_dict[x].columns = [x.lower() for x in req_dict[x].columns]
    
    if x in list(dfOnlyInLow.request_type):
        req_dict[x]['category'] = 'Only in Envs with Low Memory'
    else:
        req_dict[x]['category'] = 'Most Common in Envs with Low Memory'

conn.close()

#########################################################
##                                                     ##
##                Set up Gsheets Creds                 ##
##                                                     ##
#########################################################

file_name='Low Memory Requests {} UTC'.format(time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)
folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

gc=Client(credentials,retries=50)
output_sheets=gc.create(file_name,folder=folder)
spreadsheet = gc.open(file_name)

#########################################################
##                                                     ##
##                Create Google Sheet                  ##
##                                                     ##
#########################################################

for x in req_dict.keys():
    try: 
        gs_df = req_dict[x].copy()
        gs_df.loc[np.logical_not(gs_df.module_components.isna()),['module_components']] = gs_df.loc[np.logical_not(gs_df.module_components.isna())].module_components.apply(lambda k: k[0:50000])
        gs_df.loc[np.logical_not(gs_df.submission_data.isna()),['submission_data']] = gs_df.loc[np.logical_not(gs_df.submission_data.isna())].submission_data.apply(lambda k: k[0:50000])
        spreadsheet.add_worksheet(x, rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        wks1 = spreadsheet.worksheet_by_title(x)
        wks1.set_dataframe(gs_df, start=(1,1))
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    
try:
    spreadsheet.del_worksheet(spreadsheet.sheet1)
    print('sheet successfully deleted')
except: 
    print('sheet could not be deleted')
    
#########################################################
##                                                     ##
##      Create Linear Extrapolation of Memory          ##
##                                                     ##
#########################################################
    
final3 = df.loc[df.highStd==1].copy()
final3.sort_values(['client','env','time'],inplace=True)
final3['local_max'] = final3.groupby(['client','env']).freeMem.apply(lambda x: x.rolling(10,min_periods=1,center=True).max())
final3['local_min'] = final3.groupby(['client','env']).freeMem.apply(lambda x: x.rolling(10,min_periods=1,center=True).min())
final3.loc[final3.freeMem==final3.local_max,'linear_graph'] = final3.loc[final3.freeMem==final3.local_max].local_max
final3.loc[final3.freeMem==final3.local_min,'linear_graph'] = final3.loc[final3.freeMem==final3.local_min].local_min
final3['linear_graph'] = final3.groupby(['client','env']).linear_graph.apply(lambda x: x.interpolate(method='linear')).fillna(method='bfill')
final3['slope'] = final3.groupby(['client','env']).linear_graph.apply(lambda x: x-x.shift(1)).fillna(0)
final3['slope_group'] = np.where(round(final3.slope,4)==round(final3.slope.shift(1),4),0,1)
final3['slope_group'] = final3.groupby(['client','env']).slope_group.cumsum()
final3['slope2'] = final3.groupby(['client','env','slope_group']).slope.transform('sum')
final3['slope_indicator'] = np.where(final3.slope2 <= -.05, 1, 0)