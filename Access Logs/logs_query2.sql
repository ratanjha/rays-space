with a as (
select 
    regexp_replace(v.metadata_client,'-.*','') as client,
    (case when v.metadata_client regexp '^.*-qa.*$' then concat('qa-', v.metadata_env) else v.metadata_env end) as env,
    v.elb,
    v.elb_status_code,
    v.received_bytes,
    v.request_host,
    v.request_method,
    v.request_port,
    v.request_processing_time,
    v.request_protocol,
    v.request_uri,
    v.response_processing_time,
    v.sent_bytes,
    v.ssl_protocol,
    v.target_name,
    v.target_port,
    v.target_processing_time,
    v.target_status_code,
    v.time,
    v.metadata_date as date,
    regexp_replace(regexp_substr(v.request_uri,'.*moduleId=[0-9a-z]*'),'.*moduleId=','') as moduleId
from 
    dev.harvesting.AWS_ELB_LOGS_V0 v
where 1=1
    and v.metadata_client = (%(client)s)
    and v.metadata_env = (%(env)s)
    and v.request_uri regexp '.*&moduleId.*'
    and regexp_replace(regexp_substr(v.request_uri,'.*moduleId=[0-9a-z]*'),'.*moduleId=','') = (%(moduleId)s)
    and v.time >= dateadd(year,-2, current_date())
), b as (
select 
    client_name,
    env,
    form as moduleId,
    id as submissionId,
    created_date as submissionCreatedDate
from dev.harvesting.submissions_v0
where 1=1
    and form in (select distinct moduleid from a)
), c as (
select 
    a.client,
    a.env,
    a.elb,
    a.elb_status_code,
    a.received_bytes,
    a.request_host,
    a.request_method,
    a.request_port,
    a.request_processing_time,
    a.request_protocol,
    a.request_uri,
    a.response_processing_time,
    a.sent_bytes,
    a.ssl_protocol,
    a.target_name,
    a.target_port,
    a.target_processing_time,
    a.target_status_code,
    a.time,
    a.date,
    a.moduleId,
    count(b.submissionId) as sub_count
from a, b
where 1=1
    and a.client=b.client_name
    and a.env=b.env
    and a.moduleId=b.moduleId
    and a.time >= b.submissionCreatedDate
    and a.target_status_code = '200'
group by
    a.client,
    a.env,
    a.elb,
    a.elb_status_code,
    a.received_bytes,
    a.request_host,
    a.request_method,
    a.request_port,
    a.request_processing_time,
    a.request_protocol,
    a.request_uri,
    a.response_processing_time,
    a.sent_bytes,
    a.ssl_protocol,
    a.target_name,
    a.target_port,
    a.target_processing_time,
    a.target_status_code,
    a.time,
    a.date,
    a.moduleId
order by
    a.client,
    a.env,
    a.moduleId,
    a.time
), d as (
select
    client,
    env,
    moduleid,
    date as day,
    avg(target_processing_time) as tpt_mean,
    count(target_processing_time) as tpt_size,
    avg(sub_count) as sc_mean,
    avg(sent_bytes) as sb_mean
from c
where 1=1
group by
    client,
    env,
    moduleid,
    day
order by
    client,
    env,
    moduleid,
    day
), e as (
select 
    created,
    resolved,
    key,
    regexp_replace(lower(summary),'\n',' ') as summary,
    regexp_replace(lower(description), '\n',' ') as description
from "DEV"."HARVESTING"."V_JIRA_ISSUES"
where 1=1
    and ((project = 'Dev Ops')
    or (project = 'Intake Process' and type = 'DevOps Task'))
    and resolved is not NULL
), f as (
select
    to_date(created) as created,
    key,
    summary,
    description
from e
where 1=1
    and (
    summary regexp '^.*upsize.*$'
    or description regexp '^.*upsize.*$'
    )
    and (
    summary regexp concat('^.*',(select distinct client from d), '.*',(select distinct env from d), '.*$')
    or description regexp concat('^.*',(select distinct client from d),'.*',(select distinct env from d), '.*$')
    )
)
select 
    client,
    env,
    moduleid,
    day,
    tpt_mean,
    tpt_size,
    sc_mean,
    sb_mean,
    (case when created is NULL then 0 else 1 end) as env_sizeup
from d
left join f
on d.day = f.created
where 1=1
order by
    day
;
