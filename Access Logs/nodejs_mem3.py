
"""

Author: Ray Schaub
Date: 12/8/2021
Description: Identify different memory categories and then detect "issue" request types

Packages not installed globally:
pip install datadog
pip install datadog-api-client

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import datadog
from datadog import initialize
from datadog import api
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
from pprint import pprint
from sklearn.ensemble import IsolationForest
from scipy import stats
import pandasql as ps

#########################################################
##                                                     ##
##     Set up Env Variables & set Pandas options       ##
##                                                     ##
#########################################################

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())
pd.set_option('display.float_format', lambda x: '%.6f' % x)

#########################################################
##                                                     ##
##         Set up Snowflake Connection Params          ##
##                                                     ##
#########################################################

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()
print('Client:')
client = input()
print('Env:')
env = input()
print('Create Sheet?')
csheet = input()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh='compute_wh'

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

#########################################################
##                                                     ##
##            Create Snowflake query list              ##
##                                                     ##
#########################################################
    
queries = open('/home/jovyan/personal/Ray/rays-space/Access Logs/nodejs_mem3.sql').read()

queries = [x + ';' for x in queries.split(';')]

#########################################################
##                                                     ##
##         Set up Datadog Connection Params            ##
##                                                     ##
#########################################################
    
initialize(
    api_key=os.environ.get('DD_API_KEY'),
    app_key=os.environ.get('DD_APP_KEY'),
    statsd_host="127.0.0.1",
    statsd_port=8125
)

#########################################################
##                                                     ##
##               Query the Datadog API                 ##
##                                                     ##
#########################################################

days = 30
test = api.Metric.query(start=int(time.time()) - 3600*24*(days), end=int(time.time()),
        query='avg:system.mem.pct_usable{uq-environmentname:*} by {uq-environmentname}')

test=test['series']
env_list = [x['tag_set'][0] for x in test]
df = pd.DataFrame()

for e in env_list:
    list1 = [x['pointlist'] for x in test if x['tag_set'][0]==e]
    df1 = pd.DataFrame(list1).transpose().rename(columns={0:'pointlist'})
    df1['time'] = pd.to_datetime(df1.pointlist.apply(lambda x: x[0]*1000000))
    df1['freeMem'] = df1.pointlist.apply(lambda x: x[1])
    df1['client_env'] = re.sub('uq-environmentname:','',e)
    df = df.append(df1)
    
df['client'] = df.client_env
uat_list = ['uat','qa-uat','staging','dev-uat','sandbox-uat','production','preprod-production']
for a in uat_list:
    df['client'] = np.where(df.client_env.str.contains(a),df.client_env.apply(lambda x: re.sub('-'+a,'',x)),df.client)
df['env'] = df.apply(lambda x: re.sub(x.client+'-','',x.client_env),axis=1)
   
df2 = df.loc[df.client_env==client + '-' + env].copy()

#########################################################
##                                                     ##
##             Query the Snowflake API                 ##
##                                                     ##
#########################################################

cs=conn.cursor()

params_dict={'env':env,'client':client,'client-env':client + '_' + env}

query = queries[0]

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

#########################################################
##                                                     ##
##               Find Outlier Times                    ##
##                                                     ##
#########################################################

df2['fMchng'] = df2['freeMem'].shift(-3)-df2['freeMem']
df2['fMchng'] = df2.fMchng.fillna(0)
df2['fM_iqr'] = df2.fMchng.quantile(.5) - 2.5*(df2.fMchng.quantile(.75)-df2.fMchng.quantile(.25))
df2['outlier'] = np.where(df2.fMchng < df2.fM_iqr,1,0)

#########################################################
##                                                     ##
##                Merge Data Sets                      ##
##                                                     ##
#########################################################

df3 = df2[['client','env','time','outlier','freeMem']].copy()
df3['time2'] = df3.time.shift(-1)

sqlcode = '''
select 
    r.*,
    d.outlier
from results r inner join df3 d
on d.client=r.client
where 1=1
    and d.client=r.client
    and d.env=r.env
    and r.date between d.time and d.time2
    
'''

results2 = ps.sqldf(sqlcode,locals())

#########################################################
##                                                     ##
##                 Run A/B Tests                       ##
##                                                     ##
#########################################################

results2['category'] = results2.outlier.replace({1:'lowMem',0:'highMem'})

categories = list(results2.category.unique())
frames = {}
for category in categories:
    frames[category] = results2.loc[results2.category==category]
    frames[category]['type_percent'] = frames[category].request_count/frames[category].groupby(['date']).request_count.transform('sum')    

req_list = list(frames['lowMem'].request_type.unique())
df_sigtest = pd.DataFrame(columns=['request_type','p_value'])

for x in req_list:
    welch = stats.ttest_ind(frames['highMem'].loc[frames['highMem'].request_type==x].type_percent, 
            frames['lowMem'].loc[frames['lowMem'].request_type==x].type_percent,equal_var=False)
    df_sigtest = df_sigtest.append(pd.DataFrame([x, welch.pvalue]).transpose().rename(columns={0:'request_type',1:'p_value'}))

for category in categories:
    df_sigtest = df_sigtest.merge(frames[category].groupby(['request_type']).type_percent.mean().reset_index().rename(columns={'type_percent':category}),
        how='left',on='request_type')
    
df_sigtest['ratio'] = df_sigtest.lowMem/df_sigtest.highMem
df_sigtest.sort_values('p_value',inplace=True)

#########################################################
##                                                     ##
##        Identify Key Requests That Stand out         ##
##                                                     ##
#########################################################

dfOnlyInLow = df_sigtest.loc[df_sigtest.ratio == np.inf]
dfSignificantLow = df_sigtest.loc[(df_sigtest.ratio>1)&(df_sigtest.p_value<.099)]

#########################################################
##                                                     ##
##            Setup Variables for Query                ##
##                                                     ##
#########################################################

req_list2 = list(dfSignificantLow.request_type) + list(dfOnlyInLow.request_type)

#########################################################
##                                                     ##
##             Query the Snowflake API 2               ##
##                                                     ##
#########################################################

query = queries[1]
req_dict = {}

for x in req_list2:
    params_dict={'env':env,'client':client,'client-env':client + '_' + env,'request_types':x}
    
    req_dict[x]=pd.read_sql_query(
        query,
        con=conn,
        params=params_dict,
        #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
    )

    req_dict[x].columns = [x.lower() for x in req_dict[x].columns]
    
    if x in list(dfOnlyInLow.request_type):
        req_dict[x]['category'] = 'Only in Envs with Low Memory'
    else:
        req_dict[x]['category'] = 'Most Common in Envs with Low Memory'

conn.close()

#########################################################
##                                                     ##
##                Set up Gsheets Creds                 ##
##                                                     ##
#########################################################

file_name='Low Memory Requests One Env {} UTC'.format(time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)
folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

gc=Client(credentials,retries=50)


#########################################################
##                                                     ##
##                Create Google Sheet                  ##
##                                                     ##
#########################################################

if csheet=='yes':
    output_sheets=gc.create(file_name,folder=folder)
    spreadsheet = gc.open(file_name)
    for x in req_dict.keys():
        try: 
            gs_df = req_dict[x].copy()
            gs_df.loc[np.logical_not(gs_df.module_components.isna()),['module_components']] = gs_df.loc[np.logical_not(gs_df.module_components.isna())].module_components.apply(lambda k: 
                k[0:50000])
            gs_df.loc[np.logical_not(gs_df.submission_data.isna()),['submission_data']] = gs_df.loc[np.logical_not(gs_df.submission_data.isna())].submission_data.apply(lambda k: k[0:50000])
            spreadsheet.add_worksheet(x, rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
            wks1 = spreadsheet.worksheet_by_title(x)
            wks1.set_dataframe(gs_df, start=(1,1))
            print('sheet successfully created')
        except: 
            print('sheet could not be created')
    
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')