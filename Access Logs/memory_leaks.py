
"""

Author: Ray Schaub
Date: 6/9/2021
Description: Used to find Module submissions with differing date types

Packages not installed globally:
pip install datadog

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm


#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

print('client:')
client = input()
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
print('env:')
env = input()
print('Start Date:')
startDate = input()
print('End Date:')
endDate = input()

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")


cs=conn.cursor()

params_dict={'env':env,'client':client,'startDate':startDate,'endDate':endDate}

query = open('/home/jovyan/personal/Ray/rays-space/Access Logs/memory_leaks.sql').read()

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

memLeak = pd.read_csv('/home/jovyan/personal/Ray/rays-space/Access Logs/memory_leaks2.csv',header=0)

memLeak['time'] = pd.to_datetime(memLeak.time)
memLeak.sort_values(['group','time'],inplace=True)
memLeak['value'] = memLeak.groupby('group').value.fillna(method='ffill')
memLeak['value'] = memLeak.groupby('group').value.fillna(method='bfill')
memLeak = memLeak.loc[np.logical_not(memLeak.value.isna())].copy()
memLeak['group'] = memLeak.group.apply(lambda x: re.sub('uq-environmentname:','',x))
memLeak['client'] = memLeak.group
uat_list = ['uat','qa-uat','staging','dev-uat','sandbox-uat','production','preprod-production']
for a in uat_list:
    memLeak['client'] = np.where(memLeak.group.str.contains(a),memLeak.group.apply(lambda x: re.sub('-'+a,'',x)),memLeak.client)
memLeak['env'] = memLeak.apply(lambda x: re.sub(x.client+'-','',x.group),axis=1)
memLeak2 = memLeak[(memLeak.client==client)&(memLeak.env==env)].copy()
memLeak2['time_backup'] = memLeak2.time
memLeak2['time'] = memLeak2.time.dt.tz_localize(None)
memLeak2['time'] = memLeak2.time.dt.tz_localize('US/Eastern')
memLeak2['time'] = memLeak2.time.dt.tz_convert('UTC')
memLeak2['time'] = memLeak2.time.dt.tz_localize(None)

# 50th Percentile
def q50(x):
    return x.quantile(0.5)

# 75th Percentile
def q75(x):
    return x.quantile(0.75)

# 90th Percentile
def q90(x):
    return x.quantile(0.9)

# 95th Percentile
def q95(x):
    return x.quantile(0.95)

# 99th Percentile
def q99(x):
    return x.quantile(0.99)

def fill_dates(df):
    dates = pd.date_range(df.index.min(),df.index.max(),freq='4h')
    return df.req_size.reindex(dates)

results2 = results.groupby(['client','env','request_type','metadata_date']).agg({'target_port':'size',
            'received_bytes':q50,'processing_time':q50}).rename(columns={'target_port':'req_size',
            'received_bytes':'received_bytes_q50','processing_time':'processing_time_q50'}).reset_index()

results2a = results.groupby(['client','env','request_type','metadata_date']).agg({'received_bytes':q75,
            'processing_time':q75}).rename(columns={'received_bytes':'received_bytes_q75','processing_time':'processing_time_q75'}).reset_index()

results2b = results.groupby(['client','env','request_type','metadata_date']).agg({'received_bytes':q90,
            'processing_time':q90}).rename(columns={'received_bytes':'received_bytes_q90','processing_time':'processing_time_q90'}).reset_index()

results2c = results.groupby(['client','env','request_type','metadata_date']).agg({'received_bytes':q95,
            'processing_time':q95}).rename(columns={'received_bytes':'received_bytes_q95','processing_time':'processing_time_q95'}).reset_index()

results2d = results.groupby(['client','env','request_type','metadata_date']).agg({'received_bytes':q99,
            'processing_time':q99}).rename(columns={'received_bytes':'received_bytes_q99','processing_time':'processing_time_q99'}).reset_index()

results2 = results2.merge(results2a,on=['client','env','request_type','metadata_date']).merge(results2b,on=['client','env','request_type',
                'metadata_date']).merge(results2c,on=['client','env','request_type','metadata_date']).merge(results2d,on=['client',
                'env','request_type','metadata_date'])

results2 = results2[['client','env','request_type','metadata_date','req_size']+sorted([x for x in results2.columns if 'received_bytes' in x]) +
                   sorted([x for x in results2.columns if 'processing_time' in x])]


