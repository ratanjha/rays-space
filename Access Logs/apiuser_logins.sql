with a as (
select 
    userid,
    email,
    role,
    client_name as client,
    environment as env,
    timestamp
from "DEV"."FERMENTATION"."V_DESIGNER_TRACKING_LOGINS" 
WHERE 1=1 
    and environment = 'uat' 
    and userid = 'apiuser'
    AND DATE between '2021-09-19' AND '2021-09-26'
), b as (
select 
    metadata_client as client,
    metadata_env as env,
    request_uri,
    request_method,
    user_agent,
    client_name as ip,
    time
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    and time between '2021-09-19' AND '2021-09-26'
    and request_uri regexp '^.*auth.*$'
    and metadata_client in (select client from a)
    and metadata_env in (select env from a)
) 

select 
    a.userid,
    a.email,
    a.role,
    a.client,
    a.env,
    a.timestamp,
    b.request_uri,
    b.request_method,
    b.user_agent,
    b.ip,
    b.time
from a, b
where 1=1
    and a.client=b.client
    and a.env=b.env
    and a.timestamp between dateadd(millisecond,-100,b.time) and dateadd(millisecond,0,b.time)
order by
    a.userid,
    a.email,
    a.client,
    a.env,
    a.timestamp
;
