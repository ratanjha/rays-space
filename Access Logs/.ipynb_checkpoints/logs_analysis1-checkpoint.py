
"""
Import necessary modules

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from scipy import stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from sklearn.ensemble import IsolationForest
iso = IsolationForest(contamination=.01)
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

print('client:')
client = input()
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
print('env:')
env = input()
print('request method:')
req_method = input()
print('months:')
months = input()

if client == '':
    print('# of clients:')
    client_num = input()
    client_num = int(client_num)

print('alpha:')
alpha = input()

months = int('-' + months)
alpha = float(alpha)

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

hours = 1

def one_client(client, env, conn, hours,req_method, months, alpha):
    
    cs=conn.cursor()
    
    params_dict={'lim_num':200000,'months':months,'client':client,'env':env,'req_method':req_method}

    query = """
    set (lim_num,months,test_client,test_env,req_method) = (%(lim_num)s,%(months)s,%(client)s,%(env)s,%(req_method)s);
    """

    cs.execute(query,params_dict)

    query = """
    select 
        metadata_client,
        metadata_env,
        elb,
        elb_status_code,
        received_bytes,
        request_host,
        request_method,
        request_port,
        request_processing_time,
        request_protocol,
        request_uri,
        response_processing_time,
        sent_bytes,
        ssl_protocol,
        target_name,
        target_port,
        target_processing_time,
        target_status_code,
        time,
        to_date(time) as date
    from dev.harvesting.aws_elb_logs_v0
    --sample ($lim_num rows)
    where 1=1
        and time >= dateadd(month,$months,current_timestamp())
        and metadata_client = $test_client
        and metadata_env = $test_env
        and request_uri regexp '.*&moduleId.*'
        and request_method = $req_method
    order by time desc
    limit $lim_num
    ;
    """



    results=pd.read_sql_query(
        query,
        con=conn,
        params=params_dict,
    )

    results.columns = [x.lower() for x in results.columns]
    results['moduleId'] = results.request_uri.apply(lambda x: re.sub('.*moduleId=','',re.match('.*moduleId=[0-9a-z]*',x).group(0)))
    results['mean_sent_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).sent_bytes.transform('mean')
    results['mean_received_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).received_bytes.transform('mean')
    results['mean_received_bytes_sq'] = results['mean_received_bytes']**2
    results['mean_received_bytes_sqrt'] = results['mean_received_bytes']**0.5
    results['tpt_mean'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('mean')
    results['tpt_size'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('size')
    results['tpt_size_sq'] = results['tpt_size']**2
    results['tpt_size_sqrt'] = results['tpt_size']**0.5
    results['total_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).tpt_size.transform('size')
    results.sort_values(['target_name','time'],ascending=(1,1),inplace=True)
    
    reg_vars = ['tpt_size','mean_received_bytes','total_count','moduleId'] + [x for x in results.columns if '_sq' in x]
    reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results).fit()
    prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = alpha)
    reg.summary()
    results['yhat'] = reg.predict()
    results['yhat'] = np.where(results.yhat > 0, results.yhat, 0)
    results['iv_u'] = iv_u
    results['iv_l'] = iv_l
    results['outlier2'] = np.where(results.iv_u*1 < results.target_processing_time, 1, 0)
    results['o2_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).outlier2.transform('sum')
    
    results['min_time2'] = results.time
    results2 = results.loc[results.o2_count > 1].copy()
    results2.sort_values(['target_name','time'], ascending=(1,1),inplace=True)
    results2['_row'] =  results2.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).cumcount() + 1
    results2['_row'] = np.where(results2._row==1,1,0)
    results2['_row'] = results2._row.cumsum()
    results2a = results2.loc[results2.outlier2==1].groupby(['target_name','_row',pd.Grouper(key='time',
        freq=str(hours) + 'H')]).min_time2.min().reset_index().rename(columns={'time':'time_group'})
    results2 = results2.drop('min_time2',axis=1)
    results2 = results2.merge(results2a, how='left', on=['target_name','_row'])
    results2 = results2.loc[results2.total_count>10]
    results2['tpt_ratio'] = results2.o2_count/results2.total_count

    conn.close()
    
    return(results2)


def mult_client(env, conn, hours, req_method, months, alpha, client_num):
    
    cs=conn.cursor()
    
    params_dict={'lim_num':200000,'months':months,'env':env,'req_method':req_method}

    query = """
    set (test_env,lim_num,months,req_method) = (%(env)s,%(lim_num)s,%(months)s,%(req_method)s);
    """

    cs.execute(query,params_dict)
    
    query = """
    drop table if exists test_clients;
    """

    cs.execute(query,params_dict)
    
    query = """
    create temporary table test_clients
    (
    client_name varchar,
    _row integer
    )
    ;
    """

    cs.execute(query,params_dict)
    
    query = """
    insert into test_clients
    select 
        client_name,
        row_number() over (order by client_name) as _row
    from (
    select distinct v.metadata_client as client_name
    from 
        dev.harvesting.aws_elb_logs_v0 v
    where 1=1
        and v.time >= dateadd(month, $months, current_date())
        and v.metadata_env = $test_env
    )
    ;
    """

    cs.execute(query,params_dict)
    
    query = """
    select client_name
    from test_clients
    ;
    """
    
    clients=pd.read_sql_query(
        query,
        con=conn,
        params=params_dict,
    )

    query = """
    select 
        metadata_client,
        metadata_env,
        elb,
        elb_status_code,
        received_bytes,
        request_host,
        request_method,
        request_port,
        request_processing_time,
        request_protocol,
        request_uri,
        response_processing_time,
        sent_bytes,
        ssl_protocol,
        target_name,
        target_port,
        target_processing_time,
        target_status_code,
        time,
        to_date(time) as date
    from dev.harvesting.aws_elb_logs_v0
    --sample ($lim_num rows)
    where 1=1
        and time >= dateadd(month,$months,current_timestamp())
        and metadata_client in (select client_name from test_clients where _row=%(row)s)
        and metadata_env = $test_env
        and request_uri regexp '.*&moduleId.*'
        and request_method = $req_method
    order by time desc
    limit $lim_num
    ;
    """
    
    results1 = pd.DataFrame()

    for i in range(min(client_num,len(clients))):
        params_dict={'lim_num':200000,'months':months,'env':env,'req_method':req_method,'row':i}

        results=pd.read_sql_query(
            query,
            con=conn,
            params=params_dict,
        )
        
        if len(results) > 0:

            results.columns = [x.lower() for x in results.columns]
            results['moduleId'] = results.request_uri.apply(lambda x: re.sub('.*moduleId=','',re.match('.*moduleId=[0-9a-z]*',x).group(0)))
            results['mean_sent_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).sent_bytes.transform('mean')
            results['mean_received_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).received_bytes.transform('mean')
            results['mean_received_bytes_sq'] = results['mean_received_bytes']**2
            results['mean_received_bytes_sqrt'] = results['mean_received_bytes']**0.5
            results['tpt_mean'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('mean')
            results['tpt_size'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('size')
            results['tpt_size_sq'] = results['tpt_size']**2
            results['tpt_size_sqrt'] = results['tpt_size']**0.5
            results['total_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).tpt_size.transform('size')
            results.sort_values(['target_name','time'],ascending=(1,1),inplace=True)
    
            reg_vars = ['tpt_size','mean_received_bytes','total_count','moduleId'] + [x for x in results.columns if '_sq' in x]
            reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results).fit()
            prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = alpha)
            reg.summary()
            results['yhat'] = reg.predict()
            results['yhat'] = np.where(results.yhat > 0, results.yhat, 0)
            results['iv_u'] = iv_u
            results['iv_l'] = iv_l
            results['outlier2'] = np.where(results.iv_u*1 < results.target_processing_time, 1, 0)
            results['o2_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).outlier2.transform('sum')
    
            results['min_time2'] = results.time
            results2 = results.loc[results.o2_count > 1].copy()
            
            if len(results2) > 0:
                results2.sort_values(['target_name','time'], ascending=(1,1),inplace=True)
                results2['_row'] =  results2.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).cumcount() + 1
                results2['_row'] = np.where(results2._row==1,1,0)
                results2['_row'] = results2._row.cumsum()
                results2a = results2.loc[results2.outlier2==1].groupby(['target_name','_row',pd.Grouper(key='time',
                    freq=str(hours) + 'H')]).min_time2.min().reset_index().rename(columns={'time':'time_group'})
                results2 = results2.drop('min_time2',axis=1)
                results2 = results2.merge(results2a, how='left', on=['target_name','_row'])
                results2 = results2.loc[results2.total_count>10]
                results2['tpt_ratio'] = results2.o2_count/results2.total_count
                results1 = results1.append(results2,sort=True)
                
            else: continue
        
        else: continue

    conn.close()
    
    return(results1)

if client != '':
    try:
        results2 = one_client(client, env, conn, hours,req_method, months, alpha)
        results3 = results2.groupby(['metadata_client','metadata_env','target_name','time_group','tpt_ratio']).size().reset_index().rename(columns={0:"_size"})
        print("returned results:", len(results3))
    except:
        print("No records found")

else:
    try:
        results2 = mult_client(env, conn, hours, req_method, months, alpha, client_num)
        results3 = results2.groupby(['metadata_client','metadata_env','target_name','time_group','tpt_ratio']).size().reset_index().rename(columns={0:"_size"})
        print("returned results:", len(results3))
    except:
        print("No records found")