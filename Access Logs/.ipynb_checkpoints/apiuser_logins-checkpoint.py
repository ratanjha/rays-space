
"""

Author: Ray Schaub
Date: 10/12/2021
Description: 

Packages not installed globally:
httpagentparser
ipinfo

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import httpagentparser
import ipinfo
from pprint import pprint
from sklearn.ensemble import IsolationForest

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())

alpha = .05
iso = IsolationForest(contamination=alpha)

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

cs=conn.cursor()

#params_dict={'env':env,'client':client,'startDate':startDate,'endDate':endDate}

query = open('/home/jovyan/personal/Ray/rays-space/Access Logs/apiuser_logins.sql').read()

results=pd.read_sql_query(
    query,
    con=conn,
    #params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

conn.close()

#[x['hostname'] for x in results.ip.apply(lambda x: handler.getDetails(x).all) if 'hostname' in x.keys()]
results['all'] = results.ip.apply(lambda x: handler.getDetails(x).all)
json_list = ['city','region','country','org','hostname']
for j in json_list:
    results.loc[[i for i, x in enumerate(results['all']) if j in x.keys()],j] = results.loc[[i for i, x in 
        enumerate(results['all']) if j in x.keys()]]['all'].apply(lambda x: x[j])
    
service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)
folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'
file_name='APIUSER ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S")
gc=Client(credentials,retries=50)
output_sheets=gc.create(file_name,folder=folder)
spreadsheet = gc.open(file_name)
wks1 = spreadsheet.worksheet_by_title('Sheet1')
wks1.resize(35000)
wks1.set_dataframe(results, start=(1,1))