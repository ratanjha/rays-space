
with a as (
select  
    regexp_replace(metadata_client,'-qa','') as client,
    (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) as env,
    target_name,
    target_port,
    target_status_code,
    request,
    received_bytes,
    (case
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    when request_uri is NULL and user_agent regexp '^.*Datadog.*$' then 'datadog'
    when request_uri is NULL then 'NULL'
    when request_uri = '/ping' then 'ping'
    when request_uri = '/logout' then 'logout'
    when request_uri regexp '^/auth/.*$' then 'authorization'
    when request_uri = '/fbu/uapi/pdfbar/combine' and request_method = 'POST' then 'Combine PDFs'
    when request_uri = '/fbu/uapi/image/concatenate' and request_method = 'POST' then 'Concatenate images'
    when request_uri = '/fbu/uapi/image/convert' and request_method = 'POST' then 'Convert and image or document'
    when request_uri = '/fbu/uapi/transformer/xml2js' and request_method = 'POST' then 'Convert XML to JSON'
    when request_uri = '/fbu/uapi/system/hashString' and request_method = 'POST' then 'Create has of passed string'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create module submissions from data returned by paper ingestion service'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/status$' and request_method = 'GET' then 'Get Status'
    when request_uri = '/fbu/uapi/paperIngestion/jobs' and request_method = 'POST' then 'Post an image to an external paper ingestion OCR service'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Module'
    when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Form'
    when request_uri = '/fbu/uapi/transformer' and request_method = 'POST' then 'Execute Transform with Input Data'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/api/.*$' then concat('Execute via Proxy - ',request_method)
    when request_uri = '/fbu/uapi/fileUtils/json2csv' and request_method = 'POST' then 'Generate a CSV file'
    when request_uri = '/fbu/uapi/fileUtils/json2zip' and request_method = 'POST' then 'Generate a zip file'
    when request_uri = '/fbu/uapi/contentpdf' and request_method = 'POST' then 'Generate PDF from HTML'
    when request_uri = '/fbu/uapi/pdfbar' and request_method = 'POST' then 'Generate PDF from Template (PDF Bar)'
    when request_uri = '/fbu/uapi/referstring' and request_method = 'POST' then 'Generates an encrypted referstring for authentication'
    when request_uri = '/fbu/uapi/configurationAnalysis' and request_method = 'GET' then 'Get configuration analysis results'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/distinct$' and request_method = 'GET' then 'Get Distinct Values from Reference Data'
    when request_uri regexp '^/fbu/uapi/submissions/merge/[0-9a-z]*$' and request_method = 'GET' then 'Get Merged Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission Revisions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'DELETE' then 'Delete Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'PUT' then 'Update Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/[0-9a-z\-]*$' and request_method= 'POST' then 'Create Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Worfklow Submission Revisions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'DELETE' then 'Delete Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all\\?.*$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri = '/fbu/uapi/logs/services' and request_method = 'GET' then 'Get Service Logs'
    when request_uri regexp '^/fbu/uapi/logs/services\\?.*$' and request_method = 'GET' then 'Get Service Logs'
    when request_uri = '/fbu/uapi/fileUtils/decrypt/gpg' and request_method = 'PUT' then 'GPG Decrypt a File'
    when request_uri = '/fbu/uapi/fileUtils/encrypt/gpg' and request_method = 'PUT' then 'GPG Encrypt a File'
    when request_uri regexp '^/fbu/uapi/workflow/[0-9a-z]*/handoff$' and request_method = 'PUT' then 'Handoff Submission'
    when request_uri = '/fbu/uapi/system/getIncrementalCounter' and request_method = 'GET' then 'Incremental Counter'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart$' and request_method = 'GET' then 'List of timer start nodes and statuses'
    when request_uri = '/fbu/uapi/system/getSubmissions' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/system/getSubmissions\\?.*$' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/resume/[0-9a-z]*/submission/[0-9a-z]*$' and request_method = 'GET' then 'Resume Workflow'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/run-once$' and request_method = 'POST' then 'Run a Timer Start Node Once (Test Run)'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/start$' and request_method = 'POST' then 'Start a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/stop$' and request_method = 'POST' then 'Stop a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/dataCollections/[0-9a-z]*/import$' and request_method = 'POST' then 'Update Data Collection via Import'
    when request_uri = '/fbu/uapi/excelFill' and request_method = 'POST' then 'Upload Excel Template'
    when request_uri regexp '^.*/forms/.*$' then 'form'
    when request_uri regexp '^.*/form/.*$' then 'form'
    when request_uri regexp '^.*/transformer.*$' then 'transform'
    when request_uri regexp '^.*/modules/.*/submissions$' then 'moduleSubmissions'
    when request_uri regexp '^.*/workflows/.*/submissions$' then 'workflowSubmissions'
    when request_uri regexp '^.*/promote/.*$' then 'promotion'
    when request_uri regexp '^.*/modules.*$' then 'module'
    when request_uri regexp '^.*/workflow.*$' then 'workflow'
    when request_uri regexp '^/fbu/uapi/query/.*$' then 'query'
    when request_uri regexp '^/fbu/uapi/services/.*$' then 'services'
    when request_uri = '/fbu/uapi/tracker' then 'tracker'
    when request_uri regexp '^/fbu/files/pdf/.*$' then 'pdf'
    when request_uri regexp '^/fbu/files/pdf/combined-pdf.*$' then 'combined-pdf'
    when user_agent regexp '^.*Datadog.*$' then 'datadog'
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    else 'Other'
    end) as request_type,
    (case when request_uri regexp '^.*\\?.*$' then 'True' else 'False' end) as Conditional,
    (case 
        when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*.*$' then regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/modules/[0-9a-z]*'),'/fbu/uapi/modules/','') 
        else regexp_replace(regexp_substr(request_uri,'^.*moduleId=[0-9a-z]*'),'^.*moduleId=','') 
    end) as moduleId,
    regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/workflow?/[0-9a-z-]*'),'/fbu/uapi/workflow?/','') as workflow,
    sent_bytes,
    request_method,
    user_agent,
    request_uri,
    (case 
     when target_processing_time < 0 then 0
     else target_processing_time 
     end) as processing_time,
    to_timestamp(concat(metadata_date,  ' ', to_char(hour(time)), ':', to_char(truncate(minute(time)/60)*60)), 'YYYY-MM-DD HH24:MI') as date,
    time
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    and regexp_replace(metadata_client,'-qa','') in (%(client)s)
    and (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) in (%(env)s)
    and time between %(startDate)s and %(endDate)s
    and request_method in ('GET','POST','PUT','DELETE','PATCH')
)
select 
    client,
    env,
    date,
    request_type,
    target_status_code,
    count(1) as request_count,
    avg(processing_time) as processing_time,
    avg(received_bytes) as received_bytes,
    avg(sent_bytes) as sent_bytes
from a
where 1=1
    and concat(client,'_',env) in (%(client-env)s)
group by
    client,
    env,
    date,
    request_type,
    target_status_code
order by
    client,
    env,
    date,
    request_type,
    target_status_code
;

with a as (
select  
    regexp_replace(metadata_client,'-qa','') as client,
    (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) as env,
    target_name,
    target_port,
    target_status_code,
    request,
    received_bytes,
    (case
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    when request_uri is NULL and user_agent regexp '^.*Datadog.*$' then 'datadog'
    when request_uri is NULL then 'NULL'
    when request_uri = '/ping' then 'ping'
    when request_uri = '/logout' then 'logout'
    when request_uri regexp '^/auth/.*$' then 'authorization'
    when request_uri = '/fbu/uapi/pdfbar/combine' and request_method = 'POST' then 'Combine PDFs'
    when request_uri = '/fbu/uapi/image/concatenate' and request_method = 'POST' then 'Concatenate images'
    when request_uri = '/fbu/uapi/image/convert' and request_method = 'POST' then 'Convert and image or document'
    when request_uri = '/fbu/uapi/transformer/xml2js' and request_method = 'POST' then 'Convert XML to JSON'
    when request_uri = '/fbu/uapi/system/hashString' and request_method = 'POST' then 'Create has of passed string'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create module submissions from data returned by paper ingestion service'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/status$' and request_method = 'GET' then 'Get Status'
    when request_uri = '/fbu/uapi/paperIngestion/jobs' and request_method = 'POST' then 'Post an image to an external paper ingestion OCR service'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Module'
    when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Form'
    when request_uri = '/fbu/uapi/transformer' and request_method = 'POST' then 'Execute Transform with Input Data'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/api/.*$' then concat('Execute via Proxy - ',request_method)
    when request_uri = '/fbu/uapi/fileUtils/json2csv' and request_method = 'POST' then 'Generate a CSV file'
    when request_uri = '/fbu/uapi/fileUtils/json2zip' and request_method = 'POST' then 'Generate a zip file'
    when request_uri = '/fbu/uapi/contentpdf' and request_method = 'POST' then 'Generate PDF from HTML'
    when request_uri = '/fbu/uapi/pdfbar' and request_method = 'POST' then 'Generate PDF from Template (PDF Bar)'
    when request_uri = '/fbu/uapi/referstring' and request_method = 'POST' then 'Generates an encrypted referstring for authentication'
    when request_uri = '/fbu/uapi/configurationAnalysis' and request_method = 'GET' then 'Get configuration analysis results'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/distinct$' and request_method = 'GET' then 'Get Distinct Values from Reference Data'
    when request_uri regexp '^/fbu/uapi/submissions/merge/[0-9a-z]*$' and request_method = 'GET' then 'Get Merged Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission Revisions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'DELETE' then 'Delete Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'PUT' then 'Update Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/[0-9a-z\-]*$' and request_method= 'POST' then 'Create Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Worfklow Submission Revisions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'DELETE' then 'Delete Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all\\?.*$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri = '/fbu/uapi/logs/services' and request_method = 'GET' then 'Get Service Logs'
    when request_uri regexp '^/fbu/uapi/logs/services\\?.*$' and request_method = 'GET' then 'Get Service Logs'
    when request_uri = '/fbu/uapi/fileUtils/decrypt/gpg' and request_method = 'PUT' then 'GPG Decrypt a File'
    when request_uri = '/fbu/uapi/fileUtils/encrypt/gpg' and request_method = 'PUT' then 'GPG Encrypt a File'
    when request_uri regexp '^/fbu/uapi/workflow/[0-9a-z]*/handoff$' and request_method = 'PUT' then 'Handoff Submission'
    when request_uri = '/fbu/uapi/system/getIncrementalCounter' and request_method = 'GET' then 'Incremental Counter'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart$' and request_method = 'GET' then 'List of timer start nodes and statuses'
    when request_uri = '/fbu/uapi/system/getSubmissions' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/system/getSubmissions\\?.*$' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/resume/[0-9a-z]*/submission/[0-9a-z]*$' and request_method = 'GET' then 'Resume Workflow'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/run-once$' and request_method = 'POST' then 'Run a Timer Start Node Once (Test Run)'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/start$' and request_method = 'POST' then 'Start a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/stop$' and request_method = 'POST' then 'Stop a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/dataCollections/[0-9a-z]*/import$' and request_method = 'POST' then 'Update Data Collection via Import'
    when request_uri = '/fbu/uapi/excelFill' and request_method = 'POST' then 'Upload Excel Template'
    when request_uri regexp '^.*/forms/.*$' then 'form'
    when request_uri regexp '^.*/form/.*$' then 'form'
    when request_uri regexp '^.*/transformer.*$' then 'transform'
    when request_uri regexp '^.*/modules/.*/submissions$' then 'moduleSubmissions'
    when request_uri regexp '^.*/workflows/.*/submissions$' then 'workflowSubmissions'
    when request_uri regexp '^.*/promote/.*$' then 'promotion'
    when request_uri regexp '^.*/modules.*$' then 'module'
    when request_uri regexp '^.*/workflow.*$' then 'workflow'
    when request_uri regexp '^/fbu/uapi/query/.*$' then 'query'
    when request_uri regexp '^/fbu/uapi/services/.*$' then 'services'
    when request_uri = '/fbu/uapi/tracker' then 'tracker'
    when request_uri regexp '^/fbu/files/pdf/.*$' then 'pdf'
    when request_uri regexp '^/fbu/files/pdf/combined-pdf.*$' then 'combined-pdf'
    when user_agent regexp '^.*Datadog.*$' then 'datadog'
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    else 'Other'
    end) as request_type,
    (case when request_uri regexp '^.*\\?.*$' then 'True' else 'False' end) as Conditional,
    (case 
        when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*.*$' then regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/modules/[0-9a-z]*'),'/fbu/uapi/modules/','') 
        else regexp_replace(regexp_substr(request_uri,'^.*moduleId=[0-9a-z]*'),'^.*moduleId=','') 
    end) as moduleId,
    regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/workflow?/[0-9a-z-]*'),'/fbu/uapi/workflow?/','') as workflow,
    sent_bytes,
    request_method,
    user_agent,
    request_uri,
    (case 
     when target_processing_time < 0 then 0
     else target_processing_time 
     end) as processing_time,
    to_timestamp(concat(metadata_date,  ' ', to_char(hour(time)), ':', to_char(truncate(minute(time)/60)*60)), 'YYYY-MM-DD HH24:MI') as date,
    time
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    and regexp_replace(metadata_client,'-qa','') in (%(client)s)
    and (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) in (%(env)s)
    and time between %(startDate)s and %(endDate)s
    and request_method in ('GET','POST','PUT','DELETE','PATCH')
), b as (
select
    client,
    env,
    date,
    time,
    request_type,
    target_status_code,
    sent_bytes,
    received_bytes,
    processing_time,
    (case when request_uri regexp '^/fbu/uapi/forms/.*$' then regexp_replace(regexp_substr(request_uri, '^/fbu/uapi/forms/[0-9a-z]*'),'^/fbu/uapi/forms/','')
    when request_uri regexp '^/fbu/uapi/modules/.*$' then regexp_replace(regexp_substr(request_uri, '^/fbu/uapi/modules/[0-9a-z]*'),'^/fbu/uapi/modules/','')
    else 'None' end) as moduleId,
    (case when request_uri regexp '^.*submissions/.*$' then regexp_replace(regexp_substr(request_uri, '^.*submissions/[0-9a-z]*'),'^.*submissions/','')
    else 'None' end) as submissionId,
    request_uri,
    request
from a
where 1=1
    and concat(client,'_',env) in (%(client-env)s)
    and request_type in (%(request_types)s)
), c as (
select 
    client_name as client,
    env,
    module_id,
    listagg(label || '-' || type || ':' || component || '.') within group (order by client_name, env, module_id) as components
from dev.harvesting.modules_components_v0
where 1=1
    and client_name in (select distinct client from b)
    and env in (select distinct env from b)
    and module_id in (select distinct moduleId from b)
group by
    client_name,
    env,
    module_id
), d as (
select 
    client_name as client,
    env,
    id as submission_id,
    form as module_id,
    data
from dev.harvesting.submissions_v0
where 1=1
    and client_name in (select distinct client from b)
    and env in (select distinct env from b)
    and form in (select distinct moduleId from b)
    and id in(select distinct submissionId from b)
), e as (
select 
    client_name as client,
    env,
    id as submission_id,
    form as module_id,
    data,
    row_number() over (partition by client_name, env, form order by id) as rownum
from dev.harvesting.submissions_v0
where 1=1
    and client_name in (select distinct client from b)
    and env in (select distinct env from b)
    and form in (select distinct moduleId from b)
    and form not in (select distinct module_id from d)
), f as (
select
    client,
    env,
    submission_id,
    module_id,
    data
from e
where 1=1
and rownum = 1
), g as (
select 
    b.client,
    b.env,
    b.date,
    b.time,
    b.request_type,
    b.target_status_code,
    b.moduleId,
    b.sent_bytes,
    b.received_bytes,
    b.processing_time,
    b.submissionId,
    b.request_uri,
    b.request,
    c.components as module_components,
    d.data as submission_data
from b, c, d
where 1=1
    and b.client=c.client
    and b.client=d.client
    and b.env=c.env
    and b.env=d.env
    and b.moduleId=c.module_id
    and b.moduleId=d.module_id
    and b.submissionId=d.submission_id
union all
select 
    b.client,
    b.env,
    b.date,
    b.time,
    b.request_type,
    b.target_status_code,
    b.moduleId,
    b.sent_bytes,
    b.received_bytes,
    b.processing_time,
    b.submissionId,
    b.request_uri,
    b.request,
    c.components as module_components,
    f.data as submission_data
from b, c, f
where 1=1
    and b.client=c.client
    and b.client=f.client
    and b.env=c.env
    and b.env=f.env
    and b.moduleId=c.module_id
    and b.moduleId=f.module_id
    and b.submissionId='None'
    and b.request_type regexp '^.*Submission.*$'
union all
select 
    b.client,
    b.env,
    b.date,
    b.time,
    b.request_type,
    b.target_status_code,
    b.moduleId,
    b.sent_bytes,
    b.received_bytes,
    b.processing_time,
    b.submissionId,
    b.request_uri,
    b.request,
    c.components as module_components,
    NULL as submission_data
from b, c
where 1=1
    and b.client=c.client
    and b.env=c.env
    and b.moduleId=c.module_id
    and b.submissionId='None'
union all
select 
    b.client,
    b.env,
    b.date,
    b.time,
    b.request_type,
    b.target_status_code,
    b.moduleId,
    b.sent_bytes,
    b.received_bytes,
    b.processing_time,
    b.submissionId,
    b.request_uri,
    b.request,
    NULL as module_components,
    NULL as submission_data
from b
where 1=1
    and b.moduleId='None'
    and b.submissionId='None'
), h as (
select 
    client,
    env,
    date,
    time,
    request_type,
    target_status_code,
    sent_bytes,
    received_bytes,
    processing_time,
    moduleId,
    submissionId,
    request_uri,
    request,
    module_components,
    submission_data,
    row_number() over (partition by client, env, request_type order by time) as rownum
from g
where 1=1
)
select 
    client,
    env,
    date,
    time,
    request_type,
    target_status_code,
    sent_bytes,
    received_bytes,
    processing_time,
    moduleId,
    submissionId,
    request_uri,
    request,
    module_components,
    submission_data
from h
where 1=1
    and rownum <= 10
;
