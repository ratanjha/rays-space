
"""
Import necessary modules


sort, filter, limit, skip
collection size with similar queries and increasing processing time over time then query performance issue
identify modules that cause issues (server side execute??)
inherent to the platform??
devops had to upgrade the env to solve for the slowness of the query??

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from scipy import stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from sklearn.ensemble import IsolationForest
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

client = 'voya'
env = 'staging'
moduleId = '5e418e8a3605d5020e31d7fe'
alpha = .05
iso = IsolationForest(contamination=alpha)
outlier_coeff = 1.1
params_dict={'client':client,'env':env,'moduleId':moduleId}

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

cs=conn.cursor()

query=open('/home/jovyan/personal/Ray/Access Logs/logs_query2.sql').read()

results=pd.read_sql_query(
        query,
        con=conn,
        params=params_dict
    )

results.columns = [x.lower() for x in results.columns]

"""
results2 = results.groupby(['client','env','moduleid',pd.Grouper(key='time',freq='1d')]).agg({'target_processing_time':['mean','size'],
            'sub_count':'mean','received_bytes':'mean','sent_bytes':'mean'}).reset_index()
results2.columns = ['client','env','moduleid','day','tpt_mean','tpt_size','sc_mean','rb_mean','sb_mean']
"""
results2 = results.copy()
results2['tpt_size_sq'] = results2.tpt_size**2
results2['tpt_size_sqrt'] = results2.tpt_size**.5
results2['sc_mean_sq'] = results2.sc_mean**2
results2['sc_mean_sqrt'] = results2.sc_mean**.5
#results2['rb_mean_sq'] = results2.rb_mean**2
#results2['rb_mean_sqrt'] = results2.rb_mean**.5
results2['sb_mean_sq'] = results2.sb_mean**2
results2['sb_mean_sqrt'] = results2.sb_mean**.5
#results2['env_upsize'] = results2.groupby(['client','env','moduleid']).env_sizeup.transform(lambda x: x.rolling(5).max().shift(-4)).fillna(0)
results2['env_upsize'] = results2.env_sizeup

reg_vars = ['tpt_size','sc_mean','sb_mean','env_upsize'] + [x for x in results2.columns if '_sq' in x]
reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results2).fit()
prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = alpha)
reg.summary()
results2['yhat'] = reg.predict()
results2['yhat'] = np.where(results2.yhat > 0, results2.yhat, 0)
results2['iv_u'] = iv_u
results2['iv_l'] = iv_l
results2['reg_outlier'] = np.where(results2.iv_u*1 < results2.tpt_mean, 1, 0)


"""
reg_vars = ['tpt_size','sub_count','tpt_size_sq','tpt_size_sqrt','sc_sq','sc_sqrt']
reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results).fit()
prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = alpha)
reg.summary()
results['yhat'] = reg.predict()
results['yhat'] = np.where(results.yhat > 0, results.yhat, 0)
results['iv_u'] = iv_u
results['iv_l'] = iv_l
results['reg_outlier'] = np.where(results.iv_u*1 < results.target_processing_time, 1, 0)
results['ro_count'] = results.groupby([pd.Grouper(key='time',freq='10min')]).reg_outlier.transform('sum')
results.sort_values('time',ascending=1,inplace=True)
results['pk'] =  results.groupby([pd.Grouper(key='time',freq='10min')]).cumcount() + 1
results['pk'] = np.where(results.pk==1,1,0)
results['pk'] = results.pk.cumsum()

"""

