
"""

Author: Ray Schaub
Date: 9/15/2021
Description: 

Packages not installed globally:
pip install datadog
pip install datadog-api-client

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import datadog
from datadog import initialize
from datadog import api
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
from pprint import pprint
from sklearn.ensemble import IsolationForest
import httpagentparser

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())

alpha = .05
iso = IsolationForest(contamination=alpha)

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

initialize(
    api_key=os.environ.get('DD_API_KEY'),
    app_key=os.environ.get('DD_APP_KEY'),
    statsd_host="127.0.0.1",
    statsd_port=8125
)

cs=conn.cursor()

params_dict={'days':-30}

query = """
select 
    --metadata_client,
    --metadata_env,
    --request_method,
    --metadata_date,
    user_agent,
    count(1) as req_count
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    --and time between '10/1/2021 12:30:00' and '10/1/2021 20:30:00'
    and time >= dateadd(month,%(days)s,current_date())
    --and request_uri regexp '^/auth.*$'
    --and user_agent regexp '^.*Datadog.*$'
    --and request_URI is NULL
    --and metadata_client regexp '^ssgm.*$'
    --and metadata_env='uat'
    and user_agent is not NULL
group by
    --metadata_client,
    --metadata_env,
    --request_method,
    --metadata_date,
    user_agent
;
"""

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

conn.close()