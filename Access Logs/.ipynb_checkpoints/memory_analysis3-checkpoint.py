
"""

Author: Ray Schaub
Date: 9/15/2021
Description: 

Packages not installed globally:
pip install datadog
pip install datadog-api-client

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import datadog
from datadog import initialize
from datadog import api
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
from pprint import pprint
from sklearn.ensemble import IsolationForest

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())

alpha = .05
iso = IsolationForest(contamination=alpha)

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

initialize(
    api_key=os.environ.get('DD_API_KEY'),
    app_key=os.environ.get('DD_APP_KEY'),
    statsd_host="127.0.0.1",
    statsd_port=8125
)

big_list = api.Metric.list(int(time.time()) - 3600)
values = [x for x in big_list['metrics'] if x.startswith('system.') and 'mem.' in x]
days = 30
#values = values[:5]
value = values[0]
start = time.time()
test2 = []
#start = (datetime(2021,9,10,4,36) - datetime(1970,1,1)).total_seconds()
for i in range(0,days+1):
    test = api.Metric.query(start=int(start) - 3600*24*(i), end=int(start) - 3600*24*((i-1)),
        query='avg:'+value+'{uq-environmentname:*} by {uq-environmentname}.rollup(3600)')
    test2.extend(test['series'])

env_list = [x['tag_set'][0] for x in test2]
env_list = list(set(env_list))
df = pd.DataFrame()

for e in env_list:
    list1 = [x['pointlist'] for x in test2 if x['tag_set'][0]==e]
    flat_list = [item for sublist in list1 for item in sublist]
    df1 = pd.DataFrame(flat_list).rename(columns={0:'time',1:value})
    df1['time'] = pd.to_datetime(df1['time'].apply(lambda x: x*1000000))
    df1['client_env'] = re.sub('uq-environmentname:','',e)
    df = df.append(df1)

final = df.copy()
for value in values[1:]:
    test2=[]
    for i in range(0,days+1):
        test = api.Metric.query(start=int(start) - 3600*24*(i), end=int(start) - 3600*24*((i-1)),
            query='avg:'+value+'{uq-environmentname:*} by {uq-environmentname}.rollup(3600)')
        test2.extend(test['series'])

    env_list = [x['tag_set'][0] for x in test2]
    env_list = list(set(env_list))
    df = pd.DataFrame()

    for e in env_list:
        try:
            list1 = [x['pointlist'] for x in test2 if x['tag_set'][0]==e]
            flat_list = [item for sublist in list1 for item in sublist]
            df1 = pd.DataFrame(flat_list).rename(columns={0:'time',1:value})
            df1['time'] = pd.to_datetime(df1['time'].apply(lambda x: x*1000000))
            df1['client_env'] = re.sub('uq-environmentname:','',e)
            df = df.append(df1)
        except:
            print(value)
    try:
        final = final.merge(df,on=['client_env','time'])
    except:
        print(value)

final['min_mem'] = final.groupby(['client_env'])['system.mem.pct_usable'].transform('min')
env_list = random.sample(list(final.loc[final.client_env.str.contains('staging')].client_env.unique()),20)
mem = final.loc[final.client_env.isin(env_list)].copy()

mem.sort_values(['client_env','time'],ascending=(1,1),inplace=True)

mem['client'] = mem.client_env
uat_list = ['uat','qa-uat','staging','dev-uat','sandbox-uat','production','preprod-production']
for a in uat_list:
    mem['client'] = np.where(mem.client_env.str.contains(a),mem.client_env.apply(lambda x: re.sub('-'+a,'',x)),mem.client)
    
mem['env'] = mem.apply(lambda x: re.sub(x.client+'-','',x.client_env),axis=1)
mem.rename(columns={'time':'date'},inplace=True)

client = list(mem.client.unique())
env = list(mem.env.unique())

if 'qa-' in env:
    env = re.sub('qa-','',env)
    client = client + '-qa'

cs=conn.cursor()

params_dict={'env':env,'client':client}

query = open('/home/jovyan/personal/Ray/rays-space/Access Logs/reqs_by_type3.sql').read()

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

conn.close()

results['target_status_code2'] = results.target_status_code.apply(lambda x: x[0])
results['pt_cat'] = np.where(results.pt_50 < .1, 'Low', 'Medium')
results['pt_cat'] = np.where((results.pt_cat == 'Medium')&(results.pt_50 > 2), 'High', results.pt_cat)
results['sb_cat'] = np.where(results.sb_50 < 1024, 'Low', 'Medium')
results['sb_cat'] = np.where((results.sb_cat == 'Medium')&(results.sb_50 > 1024*5), 'High', results.pt_cat)
results['moduleid'] = np.where(results.moduleid.isna(), 'None', results.moduleid)
results['total_count'] = results.groupby(['target_status_code2','request_type','moduleid']).req_count.transform('size')

results2 = results.groupby(['client','env','request_type','target_status_code',pd.Grouper(key='date',
    freq='1h')])[list(results.columns[8:len(results)])].mean().reset_index()
results2['target_status_code2'] = results2.target_status_code.apply(lambda x: x[0])