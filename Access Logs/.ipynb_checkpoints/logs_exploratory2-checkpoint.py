
"""
Import necessary modules

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from scipy import stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from sklearn.ensemble import IsolationForest
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

alpha = .05
iso = IsolationForest(contamination=alpha)
outlier_coeff = 1.1

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

cs=conn.cursor()

query="""
select 
    metadata_client as client,
    metadata_env as env,
    elb,
    elb_status_code,
    received_bytes,
    request_host,
    request_method,
    request_port,
    request_processing_time,
    request_protocol,
    request_uri,
    response_processing_time,
    sent_bytes,
    ssl_protocol,
    target_name,
    target_port,
    target_processing_time,
    target_status_code,
    time,
    metadata_date as date
from dev.harvesting.AWS_ELB_LOGS_V0
where 1=1
    and time >= dateadd(day,-65,current_timestamp())
    and time <= dateadd(day,2,dateadd(day,-65,current_timestamp()))
    and metadata_env = 'uat'
    and request_uri regexp '.*&moduleId.*'
    and request_processing_time >= 0
order by
    time
;
"""

results=pd.read_sql_query(
        query,
        con=conn
    )

results.columns = [x.lower() for x in results.columns]
results['moduleId'] = results.request_uri.apply(lambda x: re.sub('.*moduleId=','',re.match('.*moduleId=[0-9a-z]*',x).group(0)))
results['mean_sent_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='1min')]).sent_bytes.transform('mean')
results['mean_received_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).received_bytes.transform('mean')
results['mean_received_bytes_sq'] = results['mean_received_bytes']**2
results['mean_received_bytes_sqrt'] = results['mean_received_bytes']**0.5
results['tpt_mean'] = results.groupby(['target_name',pd.Grouper(key='time',freq='1min')]).target_processing_time.transform('mean')
results['tpt_size'] = results.groupby(['target_name',pd.Grouper(key='time',freq='1min')]).target_processing_time.transform('size')
results['tpt_size_sq'] = results['tpt_size']**2
results['tpt_size_sqrt'] = results['tpt_size']**0.5
results['total_count'] = results.groupby([pd.Grouper(key='time',freq='10min')]).tpt_size.transform('size')
results.sort_values(['target_name','time'],ascending=(1,1),inplace=True)

reg_vars = ['tpt_size','mean_received_bytes','target_name'] + [x for x in results.columns if '_sq' in x]
reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results).fit()
prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = alpha)
reg.summary()
results['yhat'] = reg.predict()
results['yhat'] = np.where(results.yhat > 0, results.yhat, 0)
results['iv_u'] = iv_u
results['iv_l'] = iv_l
results['reg_outlier'] = np.where(results.iv_u*1 < results.target_processing_time, 1, 0)
results['ro_count'] = results.groupby([pd.Grouper(key='time',freq='10min')]).reg_outlier.transform('sum')
results.sort_values('time',ascending=1,inplace=True)
results['pk'] =  results.groupby([pd.Grouper(key='time',freq='10min')]).cumcount() + 1
results['pk'] = np.where(results.pk==1,1,0)
results['pk'] = results.pk.cumsum()

results2 = results.groupby(['pk',pd.Grouper(key='time',freq='10min')]).agg({'reg_outlier':['size','sum']}).reset_index()
results2.columns = ['pk','time_group','total_size','ro_size']
results2 ['ro_ratio'] = results2.ro_size/results2.total_size
ro_iqr = results2.ro_ratio.mean() + outlier_coeff*results2.ro_ratio.quantile(q=.75)
results2['ro_outlier'] = np.where(results2.ro_ratio > ro_iqr, 1, 0)

or_pks = list(results2.loc[results2.ro_outlier==1].pk)

def chart_funct(pk):
    chart = results.loc[results.pk==pk]
    group_var = ['reg_outlier']
    groups = chart.groupby(group_var)
    x = 'time'
    y = 'target_processing_time'
    fig, ax = plt.subplots(figsize=(8,6))
    for name, group in groups:
        ax.plot(group[x], group[y], marker='o', linestyle='', ms=2, label=name)
    plt.xticks(rotation=60)
    ax.legend(loc='best')
    table = chart.groupby('reg_outlier').target_processing_time.describe()
    return(plt,table)

