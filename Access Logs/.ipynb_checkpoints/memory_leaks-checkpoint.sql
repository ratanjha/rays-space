
select  
    regexp_replace(metadata_client,'-qa','') as client,
    (case when metadata_client regexp '^.*-qa$' then concat('qa-', metadata_env) else metadata_env end) as env,
    target_name,
    target_port,
    target_status_code,
    request,
    received_bytes,
    (case
    when request_uri is NULL then 'NULL'
    when request_uri = '/ping' then 'ping'
    when request_uri = '/logout' then 'logout'
    when request_uri regexp '^/auth/.*?' then 'authorization'
    when request_uri = '/fbu/uapi/pdfbar/combine' and request_method = 'POST' then 'Combine PDFs'
    when request_uri = '/fbu/uapi/image/concatenate' and request_method = 'POST' then 'Concatenate images'
    when request_uri = '/fbu/uapi/image/convert' and request_method = 'POST' then 'Convert and image or document'
    when request_uri = '/fbu/uapi/transformer/xml2js' and request_method = 'POST' then 'Convert XML to JSON'
    when request_uri = '/fbu/uapi/system/hashString' and request_method = 'POST' then 'Create has of passed string'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create module submissions from data returned by paper ingestion service'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/status$' and request_method = 'GET' then 'Get Status'
    when request_uri = '/fbu/uapi/paperIngestion/jobs' and request_method = 'POST' then 'Post an image to an external paper ingestion OCR service'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Module'
    when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Form'
    when request_uri = '/fbu/uapi/transformer' and request_method = 'POST' then 'Execute Transform with Input Data'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/api/.*$' then concat('Execute via Proxy - ',request_method)
    when request_uri = '/fbu/uapi/fileUtils/json2csv' and request_method = 'POST' then 'Generate a CSV file'
    when request_uri = '/fbu/uapi/fileUtils/json2zip' and request_method = 'POST' then 'Generate a zip file'
    when request_uri = '/fbu/uapi/contentpdf' and request_method = 'POST' then 'Generate PDF from HTML'
    when request_uri = '/fbu/uapi/pdfbar' and request_method = 'POST' then 'Generate PDF from Template (PDF Bar)'
    when request_uri = '/fbu/uapi/referstring' and request_method = 'POST' then 'Generates an encrypted referstring for authentication'
    when request_uri = '/fbu/uapi/configurationAnalysis' and request_method = 'GET' then 'Get configuration analysis results'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/distinct$' and request_method = 'GET' then 'Get Distinct Values from Reference Data'
    when request_uri regexp '^/fbu/uapi/submissions/merge/[0-9a-z]*$' and request_method = 'GET' then 'Get Merged Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission Revisions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'DELETE' then 'Delete Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'PUT' then 'Update Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/[0-9a-z\-]*$' and request_method= 'POST' then 'Create Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Worfklow Submission Revisions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'DELETE' then 'Delete Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all\\?.*$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri = '/fbu/uapi/logs/services' and request_method = 'GET' then 'Get Service Logs'
    when request_uri regexp '^/fbu/uapi/logs/services\\?.*$' and request_method = 'GET' then 'Get Service Logs'
    when request_uri = '/fbu/uapi/fileUtils/decrypt/gpg' and request_method = 'PUT' then 'GPG Decrypt a File'
    when request_uri = '/fbu/uapi/fileUtils/encrypt/gpg' and request_method = 'PUT' then 'GPG Encrypt a File'
    when request_uri regexp '^/fbu/uapi/workflow/[0-9a-z]*/handoff$' and request_method = 'PUT' then 'Handoff Submission'
    when request_uri = '/fbu/uapi/system/getIncrementalCounter' and request_method = 'GET' then 'Incremental Counter'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart$' and request_method = 'GET' then 'List of timer start nodes and statuses'
    when request_uri = '/fbu/uapi/system/getSubmissions' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/system/getSubmissions\\?.*$' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/resume/[0-9a-z]*/submission/[0-9a-z]*$' and request_method = 'GET' then 'Resume Workflow'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/run-once$' and request_method = 'POST' then 'Run a Timer Start Node Once (Test Run)'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/start$' and request_method = 'POST' then 'Start a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/stop$' and request_method = 'POST' then 'Stop a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/dataCollections/[0-9a-z]*/import$' and request_method = 'POST' then 'Update Data Collection via Import'
    when request_uri = '/fbu/uapi/excelFill' and request_method = 'POST' then 'Upload Excel Template'
    when request_uri regexp '^.*/forms/.*$' then 'form'
     when request_uri regexp '^.*/form/.*$' then 'form'
    when request_uri regexp '^.*/transformer.*$' then 'transform'
    when request_uri regexp '^.*/modules/.*/submissions$' then 'moduleSubmissions'
    when request_uri regexp '^.*/workflows/.*/submissions$' then 'workflowSubmissions'
    when request_uri regexp '^.*/promote/.*$' then 'promotion'
    when request_uri regexp '^.*/modules.*$' then 'module'
    when request_uri regexp '^.*/workflows.*$' then 'workflow'
    when request_uri regexp '^/fbu/uapi/query/.*?' then 'query'
    when request_uri regexp '^/fbu/uapi/services/.*?' then 'services'
    when request_uri = '/fbu/uapi/tracker' then 'tracker'
    when request_uri regexp '^/fbu/files/pdf/.*?' then 'pdf'
    when request_uri regexp '^/fbu/files/pdf/combined-pdf.*?' then 'combined-pdf'
    else 'Other'
    end) as request_type,
    (case when request_uri regexp '^.*\\?.*?' then 'True' else 'False' end) as Conditional,
    sent_bytes,
    request_method,
    request_uri,
    (case 
     when target_processing_time < 0 then 0
     else target_processing_time 
     end) as processing_time,
    metadata_date,
    to_timestamp(concat(metadata_date,  ' ', to_char(truncate(hour(time)/2)*2)), 'YYYY-MM-DD HH24') as date,
    time
from dev.harvesting.AWS_ELB_LOGS_V0
where 1=1
    and regexp_replace(metadata_client,'-qa','') = (%(client)s)
    and (case when metadata_client regexp '^.*-qa$' then concat('qa-', metadata_env) else metadata_env end) = (%(env)s)
    --and time >= dateadd(day,-25,current_date())
    and metadata_date between (%(startDate)s) and (%(endDate)s)
    and request_type not in ('NULL','ping','logout','authorization')
    --and request_type in ('query')
order by 
    time
;