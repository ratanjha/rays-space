
"""
Import necessary modules

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date
from datetime import timedelta
import time
import json
import getpass
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from scipy import stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from sklearn.ensemble import IsolationForest
iso = IsolationForest(contamination=.01)
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

print('client:')
client = input()
#module = '6019fe379aa63d0267a6f8fb'
#env = 'qa-uat'
print('env:')
env = input()

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

params_dict={'lim_num':200000,'months':-3,'client':client,'env':env}
cs=conn.cursor()

query = """
set (lim_num,months,test_client,test_env) = (%(lim_num)s,%(months)s,%(client)s,%(env)s);
"""

cs.execute(query,params_dict)

query = """
select 
    metadata_client,
    metadata_env,
    elb,
    elb_status_code,
    received_bytes,
    request_host,
    request_method,
    request_port,
    request_processing_time,
    request_protocol,
    request_uri,
    response_processing_time,
    sent_bytes,
    ssl_protocol,
    target_name,
    target_port,
    target_processing_time,
    target_status_code,
    time,
    to_date(time) as date
from dev.harvesting.aws_elb_logs_v0
sample ($lim_num rows)
where 1=1
    and time >= dateadd(month,$months,current_timestamp())
    and metadata_client = $test_client
    and metadata_env = $test_env
    and request_uri regexp '.*&moduleId.*'
--limit $lim_num
;
"""



results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

"""
Hours is for grouping to find high concentrations of outliers
"""

hours = 5

"""
Using rolling ST.dev
"""

results = results.loc[results.target_name != '-']
results['moduleId'] = results.request_uri.apply(lambda x: re.sub('.*moduleId=','',re.match('.*moduleId=[0-9a-z]*',x).group(0)))
results = results.sort_values(['target_name','time'], ascending=(1,1))
results['tpt_mean'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('mean')
results['tpt_size'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.transform('size')
results['tpt_rollmean'] = results.groupby(['target_name']).tpt_mean.transform(lambda x: x.rolling(50, center=True, min_periods=1).mean()).fillna(0)
results['tpt_rollstd'] = results.groupby(['target_name']).tpt_mean.transform(lambda x: x.rolling(50, center=True, min_periods=1).std()).fillna(0)
results['upper_bound'] = results.tpt_rollmean + results.tpt_rollstd*3
results['lower_bound'] = results.tpt_rollmean + results.tpt_rollstd*1
results['outlier1'] = np.where(results.tpt_mean >= results.upper_bound,1,0)
results['o1_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).outlier1.transform('sum')
results['total_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).tpt_size.transform('sum')

results1 = results.loc[results.o1_count > 1].copy()
results1 = results1.sort_values(['target_name','time'], ascending=(1,1))
results1['min_time'] = results1.time
results1['_row'] =  results1.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).cumcount() + 1
results1['_row'] = np.where(results1._row==1,1,0)
results1['_row'] = results1._row.cumsum()
results1a = results1.loc[results1.outlier1==1].groupby(['target_name','_row',pd.Grouper(key='time',freq=str(hours) + 'H')]).min_time.min().reset_index().rename(columns={'time':'time_group'})
results1 = results1.drop('min_time',axis=1)
results1 = results1.merge(results1a, how='left', on=['target_name','_row'])

"""
Using OLS
"""

results['mean_sent_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).sent_bytes.transform('mean')
results['mean_received_bytes'] = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).received_bytes.transform('mean')
results['mean_received_bytes_sq'] = results['mean_received_bytes']**2
results['mean_received_bytes_sqrt'] = results['mean_received_bytes']**0.5
results['tpt_size_sq'] = results['tpt_size']**2
results['tpt_size_sqrt'] = results['tpt_size']**0.5

reg_vars = ['tpt_size','mean_received_bytes','total_count','moduleId'] + [x for x in results.columns if '_sq' in x]
reg = sm.ols('tpt_mean~' + '+'.join(reg_vars),data=results).fit()
prstd, iv_l, iv_u = wls_prediction_std(reg, alpha = .10)
reg.summary()
results['yhat'] = reg.predict()
results['yhat'] = np.where(results.yhat > 0, results.yhat, 0)
results['iv_u'] = iv_u
results['iv_l'] = iv_l
results['outlier2'] = np.where(results.iv_u < results.tpt_mean, 1, 0)
results['o2_count'] = results.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).outlier2.transform('sum')

results['min_time2'] = results.time
results2 = results.loc[results.o2_count > 1].copy()
results2 = results2.sort_values(['target_name','time'], ascending=(1,1))
results2['_row'] =  results2.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).cumcount() + 1
results2['_row'] = np.where(results2._row==1,1,0)
results2['_row'] = results2._row.cumsum()
results2a = results2.loc[results2.outlier2==1].groupby(['target_name','_row',pd.Grouper(key='time',freq=str(hours) + 'H')]).min_time2.min().reset_index().rename(columns={'time':'time_group'})
results2 = results2.drop('min_time2',axis=1)
results2 = results2.merge(results2a, how='left', on=['target_name','_row'])

"""
results2 = results.groupby(['target_name',pd.Grouper(key='time',freq='5min')]).target_processing_time.mean().reset_index()
results2 = results2.loc[results2.target_name != '-']
results2['tpt_rollmean'] = results2.groupby(['target_name']).target_processing_time.transform(lambda x: x.rolling(500, center=True, min_periods=1).mean()).fillna(0)
results2['tpt_rollstd'] = results2.groupby(['target_name']).target_processing_time.transform(lambda x: x.rolling(500, center=True, min_periods=1).std()).fillna(0)
results2['upper_bound'] = results2.tpt_rollmean + results2.tpt_rollstd*3
results2['outlier1'] = np.where(results2.target_processing_time >= results2.upper_bound,1,0)

results3 = results2.groupby(['target_name',pd.Grouper(key='time',freq=str(hours) + 'H')]).outlier1.sum().reset_index()
results3['time2'] = results3.time + timedelta(hours=hours)


group_vars = ['elb_status_code']
results2 = results[[x for x in results.columns if 'time' in x] + group_vars].groupby(group_vars).describe()

#tresults = results.loc[(results.target_status_code=='200')].copy()
#target_results.loc[target_results.target_processing_time > 1].groupby('request_method').target_processing_time.describe()
#reg = sm.ols(formula='target_processing_time~sent_bytes+request_method+target_name',data=target_results.loc[target_results.request_method=='GET']).fit()
#reg.summary()
#target_results.loc[target_results.target_processing_time > 1].groupby('request_method').sent_bytes.describe()
#tresults.groupby(['request_method','target_name']).target_processing_time.describe().reset_index().head(50)
#getsub = results.loc[results.request_uri.str.match('.*getSubmissions.*')]
#gs_list = list(getsub.request_uri.sample(n=40,random_state=1))
#gs_list[0]
#getsub
#gbots = results.loc[results.request_uri.str.match('/robots.txt')]
#gbots.groupby('target_status_code').target_processing_time.describe()

#results.groupby('target_status_code').target_processing_time.describe().reset_index()
cols = list(results.columns)
uri_list = ['/fbu/','/api/','/auth/']
#uri_list = ['/fbu/']
uri_list = ["(^" + x + ".*)" for x in uri_list]
uri_match = '|'.join(uri_list)
results['request_legit'] = np.where(results.request_uri.str.match(uri_match),1,0)
#results.groupby('request_legit').size().reset_index()
#results.loc[(results.request_legit==1)&np.logical_not(results.request_uri.str.match('^/fbu/.*')),cols[0:15]].sample(n=20,random_state=1)
results.loc[(results.request_legit==0),cols[0:15]].sample(n=20,random_state=1)
results['module'] = np.where(results.request_uri.str.match('.*moduleId.*'),1,0)
results.groupby(['module','request_method']).size().reset_index()

sample1 = tresults.loc[(tresults.request_method=='GET')&(tresults.target_name=='172.31.15.148')].target_processing_time.copy()
sample2 = tresults.loc[(tresults.request_method=='GET')&(tresults.target_name=='172.31.47.50')].target_processing_time.copy()
stats.ttest_ind(sample1,sample2,equal_var=False)

results2 = results.loc[results.elb_status_code=='200'].copy()
time_vars = [x for x in results.columns if '_time' in x and 'target' not in x]
group_vars2 = ['request_method']
results2[time_vars + group_vars2].groupby(group_vars2).describe()

#results2.loc[results2['mean'] >= results2['mean'].quantile(.97)]
K = 400
N = 5
M = 6

results2_outlier = results2.loc[results2.target_processing_time >= results2.upper_bound].copy()
results2_outlier['time1'] = results2_outlier.time - timedelta(hours=M)
results2_outlier['time2'] = results2_outlier.time + timedelta(hours=M)
results3 = results2_outlier.groupby('target_name').size().reset_index().rename(columns={0:'_size'})
results4 = results2.loc[results2.target_name==list(results3.loc[results3._size==results3._size.max()].target_name)[0]].copy()
results4 = results4.sort_values('time',ascending=1).reset_index().drop('index',axis=1)
results2_outlier = results2_outlier.loc[results2_outlier.target_name.isin(results4.target_name)].reset_index().drop('index',axis=1)

time1 = results2_outlier.time1[N]
time2 = results2_outlier.time2[N]
results5 = results4.loc[(results4.time >= time1) & (results4.time <= time2)].copy()

x = results5.tail(K).time
y = results5.tail(K).target_processing_time
z = results5.tail(K).upper_bound

plt.plot(x, y, label='processing_time')
plt.plot(x, z, label='upper_bound')
plt.xticks(rotation=60)
plt.show()

#results.pivot_table(index='target_status_code',columns='target_name',values='target_processing_time',aggfunc='mean')
results2 = results.groupby(['target_name','target_status_code',pd.Grouper(key='time',freq='15min')]).size().reset_index().rename(columns={0:'_size'})
results2._size.describe()
results3 = results2.loc[results2.target_name=='172.31.15.148',['target_status_code','time','_size']]
width = 0.3
results3 = results3.loc[results3.target_status_code.str.match(r'(^4.*)')]
plt.bar(results3.time, results3._size, width=width)
plt.xticks(rotation=90)
plt.show()

results = results.loc[results.target_name!='-']
dummy1 = pd.get_dummies(results[['target_name']])
dummy2 = pd.get_dummies(results[['request_protocol']])
dummy3 = pd.get_dummies(results[['request_method']])
results = pd.concat([results,dummy1, dummy2, dummy3], axis=1)
results


tg = list(results1._row.unique())

chart = results1.loc[results1._row==tg[random.sample(range(len(tg)),1)[0]]]

x = chart.time
y = chart.target_processing_time
z = chart.upper_bound

print('#1:')
print(list(chart.loc[(chart.time <= chart.min_time.min())].tail(3).request_uri)[0])
print('#2:')
print(list(chart.loc[(chart.time <= chart.min_time.min())].tail(3).request_uri)[1])
print('#3:')
print(list(chart.loc[(chart.time <= chart.min_time.min())].tail(3).request_uri)[2])
print('processing time:')
print(list(chart.loc[(chart.time <= chart.min_time.min())].tail(3).target_processing_time)[2])


plt.plot(x, y, label='processing_time')
plt.plot(x, z, label='upper_bound')
plt.xticks(rotation=60)
plt.show()
chart.loc[(chart.time <= chart.min_time.min())].tail(3)
results1.total_count.min()


"""

conn.close()