with a as (
select 
    v.client_name as client,
    v.env,
    concat(v.client_name,'-',v.env) as client_env,
    v.id as workflowId,
    v.title,
    v.path,
    v.created_date
from dev.harvesting.workflows_v0 v
where 1=1
    and v.client_name in (%(client)s)
    and v.env in (%(env)s)
    and v.id in (
        select workflow_id
        from dev.harvesting.workflows_nodes_v0
        where 1=1
            and type = 'timerStart'
    )
)
select 
    client,
    env,
    client_env,
    count(1) as workflowCount
from a
where 1=1
    and client_env in (%(client_env)s)
group by
    client,
    env,
    client_env
;