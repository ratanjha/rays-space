
with a as (
select 
    issueid,
    updated,
    created,
    resolved,
    "Status" as status,
    project,
    replace(lower(summary),'\n',' ') as summary,
    replace(lower(description), '\n',' ') as description,
    "Key" as key,
    type,
    assignee,
    priority,
    creator,
    changelog_ts,
    changelog_field,
    changelog_fromstring,
    changelog_tostring,
    lag(changelog_ts) over (partition by "Key" order by changelog_ts) as lag_changelog_ts
from dev.fermentation.V_JIRA_ISSUE_AND_CHANGELOGS
where 1=1
    and ((project = 'Dev Ops')
    or (project = 'Intake Process' and type = 'DevOps Task'))
order by
    "Key",
    changelog_ts   
)
select
    issueid,
    updated,
    created,
    resolved,
    status,
    project,
    summary,
    key,
    type,
    assignee,
    priority,
    creator,
    changelog_ts,
    lag_changelog_ts,
    changelog_field,
    changelog_fromstring,
    changelog_tostring,
    (case when lag_changelog_ts is NULL then datediff(minute, created, changelog_ts) else datediff(minute, lag_changelog_ts, changelog_ts) end) as timediff
from a
where 1=1
    and (
    summary regexp '^.*promotion.*path*$'
    or summary regexp '^.*promote.*env.*$'
    or description regexp '^.*promote.*env.*$'
    or description regexp '^.*promotion.*path.*$'
    )
;
