
with a as (
select 
    issueid,
    updated,
    created,
    resolved,
    "Status" as status,
    project,
    replace(lower(summary),'\n',' ') as summary,
    replace(lower(description), '\n',' ') as description,
    key,
    type,
    assignee,
    priority,
    creator,
    changelog_ts,
    changelog_field,
    changelog_fromstring,
    changelog_tostring,
    lag(changelog_ts) over (partition by key order by changelog_ts) as lag_changelog_ts
from dev.fermentation.V_JIRA_ISSUE_AND_CHANGELOGS
where 1=1
    and ((project = 'Dev Ops')
    or (project = 'Intake Process' and type = 'DevOps Task'))
order by
    key,
    changelog_ts   
)
select
    issueid,
    updated,
    created,
    resolved,
    status,
    project,
    summary,
    key,
    type,
    assignee,
    priority,
    creator,
    changelog_ts,
    lag_changelog_ts,
    changelog_field,
    changelog_fromstring,
    changelog_tostring,
    (case when lag_changelog_ts is NULL then datediff(minute, created, changelog_ts) else datediff(minute, lag_changelog_ts, changelog_ts) end) as timediff
from a
where 1=1
    and (
    summary regexp '^.*feature.*toggle.*$'
    or summary regexp '^.*toggle.*feature.*$'
    or summary regexp '^.*dynamic.*grid.*$'
    or summary regexp '^.*alias.*custom.*$'
    or description regexp '^.*feature.*toggle.*$'
    or description regexp '^.*toggle.*feature.*$'
    or description regexp '^.*dynamic.*grid.*$'
    or description regexp '^.*alias.*custom.*$'
    or summary regexp '^.*enable.*component.*$'
    or description regexp '^.*enable.*component.*$'
    or summary regexp '^.*enable.*module.*$'
    or description regexp '^.*enable.*module.*$'
    or summary regexp '^.*enable.*workflow.*$'
    or description regexp '^.*enable.*worflow.*$'
    or summary regexp '^.*enable.*environment.*$'
    or description regexp '^.*enable.*environment.*$'
    or summary regexp '^.*enable.*promotion.*$'
    or description regexp '^.*enable.*promotion.*$'
    )
;
