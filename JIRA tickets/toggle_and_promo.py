
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
import fuzzywuzzy
from fuzzywuzzy import fuzz
import seaborn as sns

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

cs=conn.cursor()

query = open(wd + '/toggle_and_promo.sql').read()

results=pd.read_sql_query(
        query,
        con=conn
)

results.columns = [x.lower() for x in results.columns]
results['changelog_status'] = results.changelog_fromstring + '/' + results.changelog_tostring
results['hours'] = results.timediff/60
results['assignee'] = np.where(results.assignee.isna(),"Not Assigned",results.assignee)
results['created'] = pd.to_datetime(results.created,utc=True)

index = ['ticket_type','month']
results2 = results.groupby(['key','changelog_fromstring','ticket_type',pd.Grouper(key='created',freq='1m')]).hours.sum().reset_index().rename(columns={'created':'month',
        'changelog_fromstring':'status'})
results2a = results2.groupby(['ticket_type','month']).size().reset_index().rename(columns={0:'ticket_count'})
results3 = pd.pivot_table(results2, index=index,columns='status',values='hours',aggfunc='sum').reset_index().fillna(0)
results3 = results3.merge(results2a,on=['ticket_type','month'])

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

file_name='Devops Toggle Feature and Promotion Tickets {} UTC'.format(time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#spreadsheet_body=create_spreadsheet_json(results2)

def gsheets(credentials,file_name,folder):
    gc=Client(credentials,retries=50)
    output_sheets=gc.create(file_name,folder=folder)

    #spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
    spreadsheet = gc.open(file_name)
    
    try: 
        spreadsheet.add_worksheet('Changelog', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')

    wks1 = spreadsheet.worksheet_by_title('Changelog')
    wks1.set_dataframe(results3, start=(1,1))
    
