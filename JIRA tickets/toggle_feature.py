
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
import fuzzywuzzy
from fuzzywuzzy import fuzz
import seaborn as sns

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

cs=conn.cursor()

query = open(wd + '/toggle_feature.sql').read()

results=pd.read_sql_query(
        query,
        con=conn
)

results.columns = [x.lower() for x in results.columns]
results['changelog_status'] = results.changelog_fromstring + '/' + results.changelog_tostring
results['hours'] = results.timediff/60
results['assignee'] = np.where(results.assignee.isna(),"Not Assigned",results.assignee)

results2 = results.groupby(['key','type','project','status','summary','assignee','priority','creator','created']).hours.sum().reset_index()
results2['changelog_fromstring'] = 'total' 
results2['created'] = pd.to_datetime(results2.created,utc=True)
results3 = results[['key','type','project','status','summary','assignee','priority','creator','created','hours','changelog_fromstring']].append(results2,sort=True)

status_types = list(results.changelog_status.unique())
status_types2 = list(results.changelog_fromstring.unique())

results3.rename(columns={'changelog_fromstring':'changelog','status':'current_status'},inplace=True)
changelog_dict = {'Backlog':'Backlog','Blocked':'Backlog','In Dev Ops':'In Progress','Needs More Info':'Backlog','Open':'In Progress','To Do':'Backlog',
                  'Needs Review':'In Review','Needs Triage':'In Review','Ready for Support':'Done','Ready for Work':'Done'}
results3['status'] = results3.changelog.replace(changelog_dict)

results4 = results3.groupby('status').describe().reset_index()
results5 = results2.groupby([pd.Grouper(key='created',freq='1m')]).describe().reset_index()

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

file_name='Devops Toggle Feature Tickets {} UTC'.format(time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#spreadsheet_body=create_spreadsheet_json(results2)

def gsheets(credentials,file_name,folder):
    gc=Client(credentials,retries=50)
    output_sheets=gc.create(file_name,folder=folder)

    #spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
    spreadsheet = gc.open(file_name)
    
    try: 
        spreadsheet.add_worksheet('Changelog', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try: 
        spreadsheet.add_worksheet('Changelog_Month', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')

    wks1 = spreadsheet.worksheet_by_title('Changelog')
    wks1.set_dataframe(results4, start=(1,1))
    
    wks2 = spreadsheet.worksheet_by_title('Changelog_Month')
    wks2.set_dataframe(results5, start=(1,1))

"""
results = results.pivot_table(index=['key','type','project','status','summary','assignee','priority','creator','created'],
        columns='changelog_fromstring',values='hours').fillna(0).reset_index()

results['total']= results[[x for x in status_types2 if x in results.columns]].sum(axis=1)
"""