
"""
Changes

new categories: quadient, hotfix & release
Get rid of devops task in query
length, assignee, month, escalate
team responsible

"""


"""
Import necessary modules

"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import re
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime, timedelta
import time
import json
import getpass
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from scipy import stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from sklearn.ensemble import IsolationForest
iso = IsolationForest(contamination=.01)
from sklearn.decomposition import PCA
pca = PCA(n_components=2)
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
import fuzzywuzzy
from fuzzywuzzy import fuzz
import seaborn as sns
from sklearn.naive_bayes import MultinomialNB
from sklearn import preprocessing
from sklearn.pipeline import make_pipeline
from sklearn.feature_extraction.text import TfidfVectorizer

pd.set_option('display.max_colwidth', 10000)

"""
Query Snowflake DB
"""

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('Snowflake password:')
snowflake_password=getpass.getpass()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh=os.environ.get('SNOWFLAKE_WH')

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")
    
query = """
select 
    key,
    created,
    updated,
    project,
    summary,
    description,
    type,
    assignee,
    priority,
    creator,
    epic
from "DEV"."HARVESTING"."V_JIRA_ISSUES"
where 1=1
    and ((project = 'Dev Ops')
    or (project = 'Intake Process' and type = 'DevOps Task'))
order by
    key,
    updated,
    assignee
;
"""

results=pd.read_sql_query(
    query,
    con=conn
)

stop_words = stopwords.words('english')

results['SUMMARY2'] = results.SUMMARY.map(lambda x: re.sub('[,\.!?]', '', x))
results['SUMMARY2'] = results.SUMMARY2.map(lambda x: x.lower())
summary = [x.split(' ') for x in results.SUMMARY2]
summary = [item for sublist in summary for item in sublist]
#summary = [x.lower() for x in summary]
tags = nltk.pos_tag(summary)
df = pd.DataFrame(tags,columns=['names','tag'])
df2 = df.groupby(['names','tag']).size().reset_index().rename(columns={0:'occurances'})
df2.sort_values('occurances',ascending=0,inplace=True)
df2 = df2.loc[df2.tag.str.match('^N.*|^V.*|^J.*')].copy()
df2['example'] = df2.names.apply(lambda x: list(results.loc[(results.SUMMARY2.str.contains(re.escape(x)+ ' ')) | 
        (results.SUMMARY2.str.contains(re.escape(x)+ '\b'))].SUMMARY)[:1])

devops_list ="""
New Environment Creation
Certificate for Vanity URL (Custom Domain)
Upsizing Environments (Mongo and/or Compute)
SendGrid Validation Records
Create Or Modify Promotion Paths
Add (PFX) certificate bundle for mutual TLS (mTLS)
Add or modify WAF rules
Access Lists (Restricting Access By Source IP)
Create or Update Unqork-managed SFTP server
Custom “From” Email
Mongodb or Atlas access
Custom Alias For unqork.io Domain
Remove or Delete Environment
Datadog
Quadient
Hotfix & Release
"""
devops_list = devops_list.split('\n')
devops_list = [x for x in devops_list if x != '']
devops_list = [x.lower() for x in devops_list]

def lev_dist(x, y):
    max_dist = []
    for i in y:
        dist = fuzz.ratio(x,i)
        max_dist.append(dist)
    mdist = pd.DataFrame({'devops_tickets':devops_list,'distance':max_dist})
    return(list(mdist.loc[mdist['distance'] == mdist['distance'].max()].devops_tickets)[0],mdist['distance'].max())

results[['CATEGORY1', 'DISTANCE']] = results.SUMMARY2.apply(lambda x: pd.Series(lev_dist(x,devops_list)))

results['MEAN'] = results.groupby('CATEGORY1').DISTANCE.transform('mean')
results['STD'] = results.groupby('CATEGORY1').DISTANCE.transform('std')
#results['OUTLIER'] = np.where(results.MEAN + results.STD*2 < results.DISTANCE,1,0)

devops_list.sort()        

#answer = np.where((re.match(re.escape(y)+' ',a)!=None) | (re.match(re.escape(y)+'\b',a)!=None), 1, 0)
                                                           
def word_funct(a,devops_list):
    my_ratio2 = []
    for x in devops_list:
        dlist = word_tokenize(x)
        tags = nltk.pos_tag(dlist)
        dfdo = pd.DataFrame(tags,columns=['names','tag'])
        dfdo = dfdo.loc[dfdo.tag.str.match('^N.*|^V.*|^J.*')]
        dfdo['names'] = dfdo.names.apply(lambda x: re.sub('[\"\)\(]','',x))
        dfdo = dfdo.loc[np.logical_not(dfdo.names.isin(stop_words))]
        my_ratio = []
        for y in dfdo.names:
            answer = np.where((re.search(re.escape(y),a)), 1, 0)
            my_ratio.append(answer)
        my_ratio2.append(np.mean(my_ratio))
    dfdo2 = pd.DataFrame({'devops_tickets':devops_list,'ratio':my_ratio2})
    return(list(dfdo2.loc[dfdo2.ratio==dfdo2.ratio.max()].devops_tickets)[0],dfdo2.ratio.max())

results[['CATEGORY2','RATIO']] = results.SUMMARY2.apply(lambda x: pd.Series(word_funct(x,devops_list)))

devops_df = pd.DataFrame({'category':devops_list})

devops_df['keywords'] = ['whitelist','pfx|mtls|mutual.*tls','waf','vanity.*url|customer.*domain','promotion','sftp',
            'unqork\.io','welcome.*email','datadog','hotfix|release','mongodb|atlas','create.*environment|new.*environment',
            'quadient','remove.*envirnonment|delete.*environment','sendgrid|validation.*records','upsize|increase.*size']

def regex_funct(a):
    b = list(devops_df.loc[devops_df.keywords.apply(lambda x: re.search(x,a)!=None)].category)
    if len(b) == 0:
        b = 'other'
    else: 
        b = b[0]
    return(b)

results['CATEGORY3'] = results.SUMMARY2.apply(lambda x: regex_funct(x))

results['CATEGORY'] = np.where(results.RATIO==1, results.CATEGORY2,'other')
results['CATEGORY'] = np.where(results.CATEGORY=='other',results.CATEGORY3, results.CATEGORY)
results['CATEGORY'] = np.where((results.RATIO >= .5) & (results.CATEGORY=='other'), results.CATEGORY2, results.CATEGORY)
#results['CATEGORY'] = np.where(results.CATEGORY1==results.CATEGORY2, results.CATEGORY2, results.CATEGORY)
results['CATEGORY'] = np.where((results.DISTANCE > 50) & (results.CATEGORY=='other'), results.CATEGORY1, results.CATEGORY)

results2 = results.groupby('CATEGORY').DISTANCE.describe().reset_index()

results3 = results[['KEY','CREATED','UPDATED','PROJECT','SUMMARY','DESCRIPTION','TYPE','ASSIGNEE','PRIORITY','CREATOR','EPIC','CATEGORY']]
results3 = results3.loc[results3.CATEGORY=='other']
#results3 = results3.sample(200)

nb_var = 'SUMMARY2'
nb_data = results.loc[np.logical_not(results[nb_var].isna())]
train = nb_data.loc[nb_data.CATEGORY!='other'].copy()
test = nb_data.loc[nb_data.CATEGORY=='other'].copy()
model = make_pipeline(TfidfVectorizer(), MultinomialNB())
model.fit(train[nb_var], train.CATEGORY)
test['RESULTS'] = model.predict(test[nb_var])
#test[['SUMMARY','DESCRIPTION','CATEGORY1','RESULTS']]
test2 = test.loc[((test.CATEGORY1==test.RESULTS) & (test.DISTANCE >= 25)) | ((test.CATEGORY2==test.RESULTS) & (test.RATIO >=.25))]
test3 = test2.groupby('RESULTS').DISTANCE.describe().reset_index()

results = results.merge(test2[['KEY','CREATED','SUMMARY','RESULTS']], how='left', on=['KEY','CREATED','SUMMARY'])
results['CATEGORY'] = np.where(results.RESULTS.isna(),results.CATEGORY,results.RESULTS)
results = results.drop('RESULTS', axis=1)
results2 = results.groupby('CATEGORY').DISTANCE.describe().reset_index()

results4 = results.loc[results.CATEGORY=='other']
summary = [x.split(' ') for x in results4.SUMMARY2]
summary = [item for sublist in summary for item in sublist]
#summary = [x.lower() for x in summary]
tags = nltk.pos_tag(summary)
df = pd.DataFrame(tags,columns=['names','tag'])
df2 = df.groupby(['names','tag']).size().reset_index().rename(columns={0:'occurances'})
df2.sort_values('occurances',ascending=0,inplace=True)
df2 = df2.loc[df2.tag.str.match('^N.*|^V.*|^J.*')].copy()
df2['example'] = df2.names.apply(lambda x: list(results.loc[(results4.SUMMARY2.str.contains(re.escape(x)+ ' ')) | 
        (results.SUMMARY2.str.contains(re.escape(x)+ '\b'))].SUMMARY)[:1])
df2 = df2.loc[df2.occurances>=10]

service_account_info = json.loads(open('/home/jovyan/personal/raytest-312820-43948907a434.json').read())
#service_account_info = json.loads(os.environ.get('SVC_AUTH'))
scopes = ('https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive')
credentials = service_account.Credentials.from_service_account_info(service_account_info, scopes=scopes)

folder = '1mRi62UuV9RPXGmXAORTTR52utbhwTKf-'

file_name='Devops Tickets {} UTC'.format(time.strftime('%Y%m%d %H:%M:%S',time.localtime()))

#spreadsheet_body=create_spreadsheet_json(results2)

def gsheets(credentials,file_name,folder):
    gc=Client(credentials,retries=50)
    output_sheets=gc.create(file_name,folder=folder)

    #spreadsheet=gc.open_by_url('https://docs.google.com/spreadsheets/d/1E9SwEr2a_A_Niu4r2-T1xqcnHpB1R2OWQnA9kVzYkFo/edit#gid=0')
    spreadsheet = gc.open(file_name)
    
    try: 
        spreadsheet.add_worksheet('Data', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try: 
        spreadsheet.add_worksheet('Stats', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try: 
        spreadsheet.add_worksheet('Keywords', rows=100, cols=26, src_tuple=None, src_worksheet=None, index=None)
        print('sheet successfully created')
    except: 
        print('sheet could not be created')
    try:
        spreadsheet.del_worksheet(spreadsheet.sheet1)
        print('sheet successfully deleted')
    except: 
        print('sheet could not be deleted')

    wks1 = spreadsheet.worksheet_by_title('Data')
    wks1.resize(5000)
    wks1.set_dataframe(results3.sample(200), start=(1,1))
    
    wks2 = spreadsheet.worksheet_by_title('Stats')
    wks2.set_dataframe(results2, start=(1,1))
    
    wks3 = spreadsheet.worksheet_by_title('Stats')
    wks3.set_dataframe(data2, start=(1,1))