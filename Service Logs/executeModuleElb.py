"""

Author: Ray Schaub
Date: 11/23/2021
Description: Find processing times for Remote Executes by breaking down Module Composition, Submission, and DWFs, and other ELB requests

Packages not installed globally:


"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
import datadog
from datadog import initialize
from datadog import api
from pprint import pprint
from sklearn.ensemble import IsolationForest
from scipy import stats
from itertools import combinations
from sklearn.model_selection import train_test_split

#########################################################
##                                                     ##
##     Set up Env Variables & set Pandas options       ##
##                                                     ##
#########################################################

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())
pd.set_option('display.float_format', lambda x: '%.6f' % x)

#########################################################
##                                                     ##
##         Set up Datadog Connection Params            ##
##                                                     ##
#########################################################
    
initialize(
    api_key=os.environ.get('DD_API_KEY'),
    app_key=os.environ.get('DD_APP_KEY'),
    statsd_host="127.0.0.1",
    statsd_port=8125
)

#########################################################
##                                                     ##
##          Add Isolation Forests Function             ##
##                                                     ##
#########################################################

alpha = .05
iso = IsolationForest(contamination=alpha)

#########################################################
##                                                     ##
##         Set up Snowflake Connection Params          ##
##                                                     ##
#########################################################

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()
print('Client:')
client = input()
print('Env:')
#endDate = input()
#envs = ['production']
envs = input()

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh='compute_wh'

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

#########################################################
##                                                     ##
##            Create Snowflake query list              ##
##                                                     ##
#########################################################
    
queries = open('/home/jovyan/personal/Ray/rays-space/Service Logs/executeModuleElb.sql').read()

queries = [x + ';' for x in queries.split(';')]

#########################################################
##                                                     ##
##             Query the Snowflake API                 ##
##                                                     ##
#########################################################

cs=conn.cursor()

params_dict={'client':client,'env':envs}

for query in queries[:-2]:
    cs.execute(query,params_dict)

results=pd.read_sql_query(
    queries[-2:-1][0],
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

conn.close()

#########################################################
##                                                     ##
##               Query the Datadog API                 ##
##                                                     ##
#########################################################

client_env = ['uq-environmentname:' + x + '-' + envs for x in client.split(',')] 
dd_list = ' OR '.join(client_env)
big_list = api.Metric.list(int(time.time()) - 3600)
days = 28
#values = [x for x in big_list['metrics'] if re.match('^system\.mem',x) or re.match('kubernetes\.memory',x)]
values = [x for x in big_list['metrics'] if x in ['system.mem.total','system.mem.used','kubernetes.memory.limits','kubernetes.memory.usage']]
final = pd.DataFrame()
for value in values[0:]:
    test = api.Metric.query(start=int(time.time()) - 3600*24*(days), end=int(time.time()),
        query='avg:'+value+'{'+dd_list+'} by {uq-environmentname}')

    test=test['series']
    env_list = [x['tag_set'][0] for x in test]
    df = pd.DataFrame()

    for e in env_list:
        try:
            list1 = [x['pointlist'] for x in test if x['tag_set'][0]==e]
            flat_list = [item for sublist in list1 for item in sublist]
            df1 = pd.DataFrame(flat_list).rename(columns={0:'time',1:value})
            df1['time'] = pd.to_datetime(df1['time'].apply(lambda x: x*1000000))
            df1['client_env'] = re.sub('uq-environmentname:','',e)
            df = df.append(df1)
        except Exception as e:
            print('Could not append ' + value + ', error: ' + str(e))
    try:
        if len(final) > 0:
            final = final.merge(df,on=['client_env','time'],how='outer')
        else: final = df.copy()
    except Exception as e:
        print('Could not merge ' + value + ', error: ' + str(e))

"""
days = days*2
start = time.time()
#start = (datetime(2021,9,10,4,36) - datetime(1970,1,1)).total_seconds()
values = [x for x in big_list['metrics'] if re.match('^system\.mem',x) or re.match('^kubernetes\.memory',x)]
final = pd.DataFrame()
for value in values:
    test2=[]
    for i in range(0,days+1):
        test = api.Metric.query(start=int(start) - 3600*24*(i/2), end=int(start) - 3600*24*((i-1)/2),
            query='avg:'+value+'{uq-environmentname:' + client_env + '} by {uq-environmentname}.rollup(300)')
        test2.extend(test['series'])

    env_list = [x['tag_set'][0] for x in test2]
    env_list = list(set(env_list))
    df = pd.DataFrame()

    for e in env_list:
        try:
            list1 = [x['pointlist'] for x in test if x['tag_set'][0]==e]
            flat_list = [item for sublist in list1 for item in sublist]
            df1 = pd.DataFrame(flat_list).rename(columns={0:'time',1:value})
            df1['time'] = pd.to_datetime(df1['time'].apply(lambda x: x*1000000))
            df1['client_env'] = re.sub('uq-environmentname:','',e)
            df = df.append(df1)
        except Exception as e:
            print('Could not append ' + value + ', error: ' + str(e))
    try:
        if len(final) > 0:
            final = final.merge(df,on=['client_env','time'],how='outer')
        else: final = df.copy()
    except Exception as e:
        print('Could not merge ' + value + ', error: ' + str(e))
"""

final_cols = [v for v in values if v not in final.columns]
for value in final_cols:
    final[value] = None

#########################################################
##                                                     ##
##               Merge SF and DD Data                  ##
##                                                     ##
#########################################################

final['total_mem'] = np.where(final['system.mem.total'].isna(),final['kubernetes.memory.limits'],final['system.mem.total'])
final['used_mem'] = np.where(final['system.mem.used'].isna(),final['kubernetes.memory.usage'],final['system.mem.used'])
timeSeries = pd.DataFrame(pd.date_range(start=final.time.min(),end=final.time.max(),freq='5min'),columns=['time'])
timeSeries['client_env'] = final.client_env.unique()[0]
final2 = final[['client_env','time','total_mem','used_mem']].merge(timeSeries,on=['time','client_env'],how='outer')
final2['total_mem'] = final2.total_mem.fillna(method='ffill').fillna(method='bfill')
final2.sort_values('time',inplace=True)
final2 = final2.reset_index(drop=True)
final2['used_mem'] = pd.to_numeric(final2.used_mem).interpolate(method='linear',axis=0)

uat_list = ['uat','qa-uat','staging','dev-uat','sandbox-uat','production','preprod-production']
final2['client'] = None
final2.rename(columns={'time':'date'},inplace=True)
for a in uat_list:
    final2['client'] = np.where(final2.client_env.str.contains(a),final2.client_env.apply(lambda x: re.sub('-'+a,'',x)),final2.client)
final2['env'] = final2.apply(lambda x: re.sub(x.client+'-','',x.client_env),axis=1)
final2 = final2[[x for x in final2.columns if x in ['client','env','date','total_mem','used_mem']]]

#########################################################
##                                                     ##
##             Prep Data for Regression                ##
##                                                     ##
#########################################################

results2 = results.fillna(0).copy()
results2 = pd.concat([results2.iloc[:,0:3].reset_index(drop=True),results2[[x for x in results2 if x not in ['client','env','date']]].reset_index(drop=True)],axis=1)
results2.columns = [re.sub("'","",x) for x in results2.columns]
results2 = results2.merge(final2,on=['client','env','date'])
status_cols = [x for x in results2.columns if re.match('^s.00',x)]
#results2['total_req'] = results2[status_cols].sum(axis=1)
#results2 = results2[[x for x in results2.columns if x not in status_cols]]

#reg_list = [x for x in results2.columns if x not in index]
#reg_list = [re.sub('-','_',x) for x in reg_list]
#results2[[x+'_sq' for x in reg_list]] = results2[reg_list]**2
#results2[[x+'_sqrt' for x in reg_list]] = results2[reg_list]**.5
#reg_list = [x for x in results2.columns if x not in index]

index = ['client','env','date']
results2.columns = [re.sub('[- ]','_',x) for x in results2.columns]
results2.columns = [re.sub('[()]','',x) for x in results2.columns]
reg_list = [x for x in results2.columns if x not in index]
results2[reg_list] = (results2[reg_list] - results2[reg_list].mean(axis=0))/results2[reg_list].std(axis=0)
results2[reg_list] = results2[reg_list].fillna(0)
reg_list = [x for x in results2.columns if x not in index]
dep_var = 'processing_time'
reg_vars = [x for x in reg_list if x!=dep_var]
results2['outlier'] = iso.fit_predict(results2[reg_vars])

train, test = train_test_split(results2, test_size=0.2)
train = train.copy()
test = test.copy()

#########################################################
##                                                     ##
##             Run Stepwise Regression                 ##
##                                                     ##
#########################################################

reg_vars = [x for x in reg_list if x!=dep_var]
r2max = []
for rv in reg_vars:
    reg1 = sm.ols(dep_var + '~' + rv,data=train).fit()
    r2 = reg1.rsquared
    r2max.append(r2)

used_vars = [reg_vars[i] for i in range(0,len(r2max)) if r2max[i]==max(r2max)]
reg_vars = [x for x in reg_vars if x not in used_vars]
combos = list(combinations(reg_vars,2))
combos = [x + '*' + y for (x,y) in combos]
reg_vars = reg_vars + random.sample(combos,50)

reg2 = sm.ols(dep_var + '~' + '+'.join(used_vars),data=train).fit()
aicdiff = 2

while aicdiff>=2 and len(reg_vars)>=1:
    aicmin = []
    for rv in reg_vars:
        reg1 = sm.ols(dep_var + '~' + '+'.join(used_vars + [rv]),data=train).fit()
        aic = reg1.aic
        aicmin.append(aic)
    used_vars = used_vars + [reg_vars[i] for i in range(0,len(aicmin)) if aicmin[i]==min(aicmin)]
    reg_vars = [x for x in reg_vars if x not in used_vars]
    reg = sm.ols(dep_var + '~' + '+'.join(used_vars),data=train).fit()
    aicdiff = reg2.aic - reg.aic
    reg2 = reg

pvals = pd.DataFrame(reg.pvalues,columns=['pval']).reset_index()
used_vars = list(pvals.loc[pvals.pval<=.05]['index'])
#pvals.loc[np.logical_not(pvals['index'].isin([re.sub('\\*',':',x) for x in used_vars]))]
reg = sm.ols(dep_var + '~' + '+'.join(used_vars[1:]),data=train).fit()


#########################################################
##                                                     ##
##       Follow up on Results for Test Set             ##
##                                                     ##
#########################################################

params = pd.DataFrame(reg.params,columns=['param']).reset_index()
mult_list = [x for x in params['index'] if re.match('^.*:.*$',x)]
for value in mult_list:
    test[value] = test[value.split(':')[0]]*test[value.split(':')[1]]
    train[value] = train[value.split(':')[0]]*train[value.split(':')[1]]
params_list=[x for x in params['index'] if x!='Intercept']
split_list = [x.split(':') for x in params_list]
full_list = [item for sublist in split_list for item in sublist]
unique_list = list(set(full_list))
train['dotproduct'] = train[params_list].dot(params.loc[params['index']!='Intercept'].set_index('index'))+params.loc[params['index']=='Intercept'].param[0]
train['yhat'] = reg.predict(train[unique_list])
test['dotproduct'] = test[params_list].dot(params.loc[params['index']!='Intercept'].set_index('index'))+params.loc[params['index']=='Intercept'].param[0]
test['yhat'] = reg.predict(test[unique_list])

#########################################################
##                                                     ##
##          Print Results for Test Dataset             ##
##                                                     ##
#########################################################

#TSS = sum((train.processing_time-train.processing_time.mean())**2)
#RSS = sum((train.processing_time-train.yhat)**2)
TSS = sum((test.processing_time-test.processing_time.mean())**2)
RSS = sum((test.processing_time-test.yhat)**2)
print('R^2 for Test Dataset is: ', 1-RSS/TSS)