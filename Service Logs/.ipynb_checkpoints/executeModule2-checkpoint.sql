
select distinct client_name
from dev.harvesting.modules_v0
sample (10)
where 1=1
limit 5
;

with a as (
select 
    sv.client_name,
    sv.env,
    sv.id,
    sv.form_id,
    sv.submission_id,
    sv.created_date,
    sv.modified_date,
    sv.type,
    sv.url,
    --datediff(milliseconds,sv.responsetime,sv.created_date)/1000 as service_response_time,
    (json_raw:data:responseTime['$date'] - json_raw:data:requestTime['$date'])/1000 as service_response_time,
    sv.servicename,
    sv.componentkey,
    sv.method,
    sv.responsecode,
    sv.responsetime,
    sv.inserted_date
from 
    dev.harvesting.servicelogs_v0 sv
where 1=1
    and sv.created_date between %(startDate)s and %(endDate)s
    and sv.client_name in (%(client)s)
    and sv.env in (%(env)s)
    and sv.type = 'remoteExecute'
), b as (
select 
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type,
    count(1)  as component_count
from 
    dev.harvesting.archives_definitions_components_v0 dc,
    dev.harvesting.archives_definitions_v0 d
where 1=1
    and dc.fk_archive = d.pk_archive
    and d.client_name in (select distinct client_name from a)
    and d.env in (select distinct env from a)
group by
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type
), c as (
select 
    a.client_name,
    a.env,
    a.id,
    a.created_date,
    b.formid,
    a.service_response_time,
    a.servicename,
    a.componentkey,
    a.method,
    a.responsecode,
    b.created_date as archive_date,
    b.type,
    b.component_count
from a, b
where 1=1
    and a.client_name = b.client_name
    and a.env = b.env
    and a.form_id = b.formid
    and a.created_date >= b.created_date
), d as (
select 
    client_name,
    env,
    id,
    created_date,
    formid,
    service_response_time,
    servicename,
    componentkey,
    method,
    responsecode,
    type,
    component_count,
    archive_date,
    max(archive_date) over (partition by id) as max_archive_date
from c
)
select 
    client_name,
    env,
    id,
    created_date,
    formid,
    service_response_time,
    servicename,
    componentkey,
    method,
    responsecode,
    type,
    component_count
from d
where 1=1
    and archive_date = max_archive_date
order by
    client_name,
    env,
    formid,
    created_date
;
