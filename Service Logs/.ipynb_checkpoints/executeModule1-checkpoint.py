"""

Author: Ray Schaub
Date: 10/29/2021
Description: Find processing times for Remote Executes by breaking down Module Composition

Packages not installed globally:


"""

import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy import create_engine
from snowflake.sqlalchemy import URL
import snowflake.connector
import os
import random
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from pygsheets.client import Client
from datetime import date, datetime
from datetime import timedelta
import time
import json
import getpass
import itertools
import re
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from dateutil.parser import parse as dateutil_parser
from datadog_api_client.v2 import ApiClient, ApiException, Configuration
from datadog_api_client.v2.api import logs_api
from datadog_api_client.v2.models import *
from pprint import pprint
from sklearn.ensemble import IsolationForest
from scipy import stats

#########################################################
##                                                     ##
##     Set up Env Variables & set Pandas options       ##
##                                                     ##
#########################################################

exec(open('/home/jovyan/personal/Ray/setup_env.py').read())
pd.set_option('display.float_format', lambda x: '%.6f' % x)

#########################################################
##                                                     ##
##          Add Isolation Forests Function             ##
##                                                     ##
#########################################################

alpha = .05
iso = IsolationForest(contamination=alpha)

#########################################################
##                                                     ##
##         Set up Snowflake Connection Params          ##
##                                                     ##
#########################################################

#fetch snowflake username and password from environment variables
snowflake_username=os.environ.get('SNOWFLAKE_USER')
print('snowflake password:')
snowflake_password=getpass.getpass()
print('Start Date:')
startDate = input()
print('End Date:')
endDate = input()
envs = ['production']

#fetch snowflake environment info from environment variables
snowflake_host=os.environ.get('SNOWFLAKE_HOST')
snowflake_db=os.environ.get('SNOWFLAKE_DB')
snowflake_wh='compute_wh'

#create the connection to the snowflake database
try:
    snowflake.connector.paramstyle='pyformat'
    conn=snowflake.connector.connect(
        account=snowflake_host,
        warehouse=snowflake_wh,
        database=snowflake_db,
        schema='FERMENTATION',
        user=snowflake_username,
        password=snowflake_password
    )
except Exception as e:
    print("Unexpected error:", e)
    raise Exception("Invalid Snowflake Credentials")

#########################################################
##                                                     ##
##            Create Snowflake query list              ##
##                                                     ##
#########################################################
    
queries = open('/home/jovyan/personal/Ray/rays-space/Service Logs/executeModule1.sql').read()

queries = [x + ';' for x in queries.split(';')]

#########################################################
##                                                     ##
##             Query the Snowflake API                 ##
##                                                     ##
#########################################################

cs=conn.cursor()

query = queries[0]

params_dict1={'startDate':startDate,'endDate':endDate}

clients=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict1,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

clients.columns = [x.lower() for x in clients.columns]

params_dict={'env':envs,'client':list(clients.client_name),'startDate':startDate,'endDate':endDate}

query = queries[1]

results=pd.read_sql_query(
    query,
    con=conn,
    params=params_dict,
    #parse_dates={'MODIFIED_TIME_UTC':'%Y-%m-%d %H:%M:%S.%f'}
)

results.columns = [x.lower() for x in results.columns]

#########################################################
##                                                     ##
##             Prep Data for Regression                ##
##                                                     ##
#########################################################

index = ['client_name','env','id','formid','created_date','service_response_time']
results2 = pd.pivot_table(results, index=index, 
    columns='type', values='component_count', aggfunc='sum').reset_index().fillna(0)
results2.columns = [re.sub('-','_',x) for x in results2.columns]
results2['req_count'] = results2.groupby(['client_name','env','formid',pd.Grouper(key='created_date',freq='2min')]).id.transform('size')
#reg_list = [x for x in results2.columns if x not in index]
#reg_list = [re.sub('-','_',x) for x in reg_list]
#results2[[x+'_sq' for x in reg_list]] = results2[reg_list]**2
#results2[[x+'_sqrt' for x in reg_list]] = results2[reg_list]**.5
#reg_list = [x for x in results2.columns if x not in index]

results2a = pd.pivot_table(results, index=index, 
    columns='responsecode', values='component_count', aggfunc='sum').reset_index().fillna(0)
results2a.columns = [str(x) for x in results2a.columns if x in index] + ['rp' + str(x) for x in results2a.columns if x not in index]

results2 = results2.merge(results2a, on=['client_name','env','formid','created_date','service_response_time','id']).fillna(0)

reg_list = [x for x in results2.columns if x not in index]
#results2[reg_list] = (results2[reg_list] - results2[reg_list].mean())/results2[reg_list].std()
dep_var = 'service_response_time'

#########################################################
##                                                     ##
##             Run Stepwise Regression                 ##
##                                                     ##
#########################################################

reg_vars = reg_list
r2max = []
for rv in reg_vars:
    reg1 = sm.ols(dep_var + '~' + rv,data=results2).fit()
    r2 = reg1.rsquared
    r2max.append(r2)

used_vars = [reg_vars[i] for i in range(0,len(r2max)) if r2max[i]==max(r2max)]
reg_vars = [x for x in reg_vars if x not in used_vars]

reg2 = sm.ols(dep_var + '~' + '+'.join(used_vars),data=results2).fit()
aicdiff = 2

while aicdiff>=2 and len(reg_vars)>=1:
    aicmin = []
    for rv in reg_vars:
        reg1 = sm.ols(dep_var + '~' + '+'.join(used_vars + [rv]),data=results2).fit()
        aic = reg1.aic
        aicmin.append(aic)
    used_vars = used_vars + [reg_vars[i] for i in range(0,len(aicmin)) if aicmin[i]==min(aicmin)]
    reg_vars = [x for x in reg_vars if x not in used_vars]
    reg = sm.ols(dep_var + '~' + '+'.join(used_vars),data=results2).fit()
    aicdiff = reg2.aic - reg.aic
    reg2 = reg

pvals = list(reg.pvalues)
rm1 = [used_vars[i] for i in range(0,len(pvals[1:])) if pvals[1:][i]>.1 or np.isnan(pvals[1:][i])]
used_vars = [x for x in used_vars if x not in rm1]
results2['outlier'] = iso.fit_predict(results2[used_vars])
reg = sm.ols(dep_var + '~' + '+'.join(used_vars+['outlier']),data=results2).fit()