
set (client,env,req_types,comp_types,time_divider) = (%(client)s,%(env)s,
    concat('Execute Module,Execute Form,query,transform,Combine PDFs,Concatenate images,',
    'Generate a CSV file,List Submissions for Dashboard,Get Module Submissions,Get ServiceLogs,Submissions Base64,Convert XML to JSON,Get Module Submission'),
    concat('dataworkflow,datamapper,integrator,initializer,timer,',
    'transformer,viewgrid,pdf,email,datagrid,query,map,container'),5)
;

create or replace temporary table clients as
select value as client
from table(flatten(input=>split($client,',')))
;

create or replace temporary table req_types as
select value as req_type
from table(flatten(input=>split($req_types,',')))
;

create or replace temporary table comp_types as
select value as comp_type
from table(flatten(input=>split($comp_types,',')))
;

create or replace temporary table elbs as
select  
    regexp_replace(metadata_client,'-qa','') as client,
    (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) as env,
    target_name,
    target_port,
    concat('s',substring(target_status_code,1,1),'00') as status_code,
    request,
    received_bytes,
    (case
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    when request_uri is NULL and user_agent regexp '^.*Datadog.*$' then 'datadog'
    when request_uri is NULL then 'NULL'
    when request_uri = '/ping' then 'ping'
    when request_uri = '/logout' then 'logout'
    when request_uri regexp '^/auth/.*$' then 'authorization'
    when request_uri = '/fbu/uapi/pdfbar/combine' and request_method = 'POST' then 'Combine PDFs'
    when request_uri = '/fbu/uapi/image/concatenate' and request_method = 'POST' then 'Concatenate images'
    when request_uri = '/fbu/uapi/image/convert' and request_method = 'POST' then 'Convert an image or document'
    when request_uri = '/fbu/uapi/transformer/xml2js' and request_method = 'POST' then 'Convert XML to JSON'
    when request_uri = '/fbu/uapi/system/hashString' and request_method = 'POST' then 'Create has of passed string'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create module submissions from data returned by paper ingestion service'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/status$' and request_method = 'GET' then 'Get Status'
    when request_uri = '/fbu/uapi/paperIngestion/jobs' and request_method = 'POST' then 'Post an image to an external paper ingestion OCR service'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Module'
    when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Form'
    when request_uri = '/fbu/uapi/transformer' and request_method = 'POST' then 'Execute Transform with Input Data'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/api/.*$' then concat('Execute via Proxy - ',request_method)
    when request_uri = '/fbu/uapi/fileUtils/json2csv' and request_method = 'POST' then 'Generate a CSV file'
    when request_uri = '/fbu/uapi/fileUtils/json2zip' and request_method = 'POST' then 'Generate a zip file'
    when request_uri = '/fbu/uapi/contentpdf' and request_method = 'POST' then 'Generate PDF from HTML'
    when request_uri = '/fbu/uapi/pdfbar' and request_method = 'POST' then 'Generate PDF from Template (PDF Bar)'
    when request_uri = '/fbu/uapi/referstring' and request_method = 'POST' then 'Generates an encrypted referstring for authentication'
    when request_uri = '/fbu/uapi/configurationAnalysis' and request_method = 'GET' then 'Get configuration analysis results'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/distinct$' and request_method = 'GET' then 'Get Distinct Values from Reference Data'
    when request_uri regexp '^/fbu/uapi/submissions/merge/[0-9a-z]*$' and request_method = 'GET' then 'Get Merged Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission Revisions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'DELETE' then 'Delete Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'PUT' then 'Update Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/[0-9a-z\-]*$' and request_method= 'POST' then 'Create Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Worfklow Submission Revisions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'DELETE' then 'Delete Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all\\?.*$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri = '/fbu/uapi/logs/services' and request_method = 'GET' then 'Get Service Logs'
    when request_uri regexp '^/fbu/uapi/logs/services\\?.*$' and request_method = 'GET' then 'Get Service Logs'
    when request_uri = '/fbu/uapi/fileUtils/decrypt/gpg' and request_method = 'PUT' then 'GPG Decrypt a File'
    when request_uri = '/fbu/uapi/fileUtils/encrypt/gpg' and request_method = 'PUT' then 'GPG Encrypt a File'
    when request_uri regexp '^/fbu/uapi/workflow/[0-9a-z]*/handoff$' and request_method = 'PUT' then 'Handoff Submission'
    when request_uri = '/fbu/uapi/system/getIncrementalCounter' and request_method = 'GET' then 'Incremental Counter'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart$' and request_method = 'GET' then 'List of timer start nodes and statuses'
    when request_uri = '/fbu/uapi/system/getSubmissions' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/system/getSubmissions\\?.*$' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/resume/[0-9a-z]*/submission/[0-9a-z]*$' and request_method = 'GET' then 'Resume Workflow'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/run-once$' and request_method = 'POST' then 'Run a Timer Start Node Once (Test Run)'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/start$' and request_method = 'POST' then 'Start a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/stop$' and request_method = 'POST' then 'Stop a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/dataCollections/[0-9a-z]*/import$' and request_method = 'POST' then 'Update Data Collection via Import'
    when request_uri = '/fbu/uapi/excelFill' and request_method = 'POST' then 'Upload Excel Template'
    when request_uri regexp '^.*/forms/.*$' then 'form'
    when request_uri regexp '^.*/form/.*$' then 'form'
    when request_uri regexp '^.*/transformer.*$' then 'transform'
    when request_uri regexp '^.*/modules/.*/submissions$' then 'moduleSubmissions'
    when request_uri regexp '^.*/workflows/.*/submissions$' then 'workflowSubmissions'
    when request_uri regexp '^.*/promote/.*$' then 'promotion'
    when request_uri regexp '^.*/modules.*$' then 'module'
    when request_uri regexp '^.*/workflow.*$' then 'workflow'
    when request_uri regexp '^/fbu/uapi/query/.*$' then 'query'
    when request_uri regexp '^/fbu/uapi/services/.*$' then 'services'
    when request_uri = '/fbu/uapi/tracker' then 'tracker'
    when request_uri regexp '^/fbu/files/pdf/.*$' then 'pdf'
    when request_uri regexp '^/fbu/files/pdf/combined-pdf.*$' then 'combined-pdf'
    when user_agent regexp '^.*Datadog.*$' then 'datadog'
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    when request_uri regexp '^/fbu/files/submissionBase64.*$' then 'Submission Base64' 
    else 'Other'
    end) as request_type,
    (case when request_uri regexp '^.*\\?.*$' then 'True' else 'False' end) as Conditional,
    (case 
        when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*.*$' then regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/modules/[0-9a-z]*'),'/fbu/uapi/modules/','') 
        when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' then regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/forms/[0-9a-z]*'),'/fbu/uapi/forms/','') 
        else regexp_replace(regexp_substr(request_uri,'^.*moduleId=[0-9a-z]*'),'^.*moduleId=','') 
    end) as moduleId,
    regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/workflow?/[0-9a-z-]*'),'/fbu/uapi/workflow?/','') as workflow,
    sent_bytes,
    request_method,
    user_agent,
    request_uri,
    (case 
     when target_processing_time < 0 then 0
     else target_processing_time 
     end) as processing_time,
    to_timestamp(concat(metadata_date,  ' ', to_char(hour(time)), ':', to_char(truncate(minute(time)/$time_divider)*$time_divider)), 'YYYY-MM-DD HH24:MI') as date,
    time
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    --and request_uri regexp '^.*/transformer.*$'
    and time >= dateadd(week,-6,current_date())
    and regexp_replace(metadata_client,'-qa','') in (select client from clients)
    and (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) = $env
;

select
    client,
    env,
    date,
    status_code as pivot_column,
    count(1) as pivot_value
from elbs
where 1=1
group by
    client,
    env,
    date,
    status_code
;

call pivot_prev_results('sum');

create or replace temporary table status1 as
select *
from table(result_scan(last_query_id(-2)))
;

select
    client,
    env,
    date,
    request_type as pivot_column,
    count(1) as pivot_value
from elbs
where 1=1
    and request_type in (select req_type from req_types)
group by
    client,
    env,
    date,
    request_type
;

call pivot_prev_results('sum');

create or replace temporary table request1 as
select *
from table(result_scan(last_query_id(-2)))
;

create or replace temporary table request2 as
select
    client,
    env,
    date,
    avg(received_bytes) as received_bytes,
    avg(sent_bytes) as sent_bytes,
    sum(zeroifNULL(regexp_count(case when request_uri regexp '^.*\\?.*$' then regexp_replace(request_uri,'^.*\\?','') else NULL end,'='))) as clause_count
from elbs
where 1=1   
group by
    client,
    env,
    date
;

create or replace temporary table target as
select
    client,
    env,
    date,
    avg(processing_time) as processing_time
from elbs
where 1=1   
group by
    client,
    env,
    date
;

create or replace temporary table modules1 as
select
    client,
    env,
    date,
    request_type,
    moduleId,
    count(1) as reqMod_count
from elbs
where 1=1   
    and request_type in ('Execute Module','Execute Form')
group by
    client,
    env,
    date,
    request_type,
    moduleId
;

create or replace temporary table modules2 as
select 
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type,
    count(1)  as component_count
from 
    dev.harvesting.archives_definitions_components_v0 dc,
    dev.harvesting.archives_definitions_v0 d
where 1=1
    and dc.fk_archive = d.pk_archive
    and d.client_name in (select client from clients)
    and d.env = $env
    and dc.type in (select comp_type from comp_types)
group by
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type
;

create or replace temporary table modules3 as
select
    m1.client,
    m1.env,
    m1.moduleId,
    m1.date,
    m2.created_date,
    m1.reqMod_count,
    max(m2.created_date) over (partition by m1.client, m1.env, m1.moduleId) as max_date,
    m2.type,
    m2.component_count
from 
    modules1 m1,
    modules2 m2
where 1=1
    and m1.client=m2.client_name
    and m2.env=m2.env
    and m1.moduleId=m2.formId
    and m1.date>=m2.created_date
;

select 
    client,
    env,
    date,
    type as pivot_column,
    component_count as pivot_value
from modules3
where 1=1
    and created_date=max_date
;

call pivot_prev_results('sum');

create or replace temporary table modules4 as
select *
from table(result_scan(last_query_id(-2)))
;

create or replace temporary table dwf1 as
select 
    d.client_name,
    d.env,
    d.formId,
    d.created_date,
    d.modified_date,
    regexp_replace(regexp_substr(c.path,'^dataworkflowData\.data.*Object'),'^dataworkflowData\.','') as dwf_type,
    count(1) as dwf_count
from 
    dev.harvesting.archives_definitions_components_v0 dc,
    dev.harvesting.archives_definitions_v0 d,
    lateral flatten(input=>dc.component, recursive=>True) c
where 1=1
    and dc.fk_archive = d.pk_archive
    and d.client_name in (select client from clients)
    and d.env = $env
    and (c.path regexp '^dataworkflowData.data.*Object\[[0-9]*\]\.id$' or c.path regexp '^dataworkflowData.data.*Object\[[0-9]*\]\.dataOperation$')
    and len(c.value) > 0
group by
    d.client_name,
    d.env,
    d.formId,
    d.created_date,
    d.modified_date,
    regexp_replace(regexp_substr(c.path,'^dataworkflowData\.data.*Object'),'^dataworkflowData\.','')
;

create or replace temporary table dwf2 as
select
    m1.client,
    m1.env,
    m1.moduleId,
    m1.date,
    d1.modified_date,
    m1.reqMod_count,
    max(d1.modified_date) over (partition by m1.client, m1.env, m1.moduleId) as max_date,
    d1.dwf_type,
    d1.dwf_count
from 
    modules1 m1,
    dwf1 d1
where 1=1
    and m1.client=d1.client_name
    and m1.env=d1.env
    and m1.moduleId=d1.formId
    and m1.date>=d1.modified_date
;

select 
    client,
    env,
    date,
    dwf_type as pivot_column,
    dwf_count as pivot_value
from dwf2
where 1=1
    and modified_date=max_date
;

call pivot_prev_results('sum');

create or replace temporary table dwf3 as
select *
from table(result_scan(last_query_id(-2)))
;

create or replace temporary table submissions1 as
select
    client,
    env,
    date,
    request_type,
    moduleId,
    count(1) as getSub_count
from elbs
where 1=1   
    and request_type in ('Get Module Submissions','Get Module Submission','List Submissions for Dashboard')
group by
    client,
    env,
    date,
    request_type,
    moduleId
;

create or replace temporary table submissions2 as
select 
    s1.client,
    s1.env,
    s1.date,
    count(1)  as sub_count
from 
    dev.harvesting.submissions_v0 s,
    submissions1 s1
where 1=1
    and s1.moduleId=s.form
    and s.client_name=s1.client
    and s.env=s1.env
    and s1.date >= s.created_date
    and s.client_name in (select client from clients)
    and s.env = $env
group by
    s1.client,
    s1.env,
    s1.date
;


select *
from status1 s left join request1 r
    on s.client=r.client
    and s.env=r.env
    and s.date=r.date
left join modules4 m
    on s.client=m.client
    and s.env=m.env
    and s.date=m.date
left join dwf3 d
    on s.client=d.client
    and s.env=d.env
    and s.date=d.date
left join submissions2 su
    on s.client=su.client
    and s.env=su.env
    and s.date=su.date
left join request2 r2
    on s.client=r2.client
    and s.env=r2.env
    and s.date=r2.date
left join target t
    on s.client=t.client
    and s.env=t.env
    and s.date=t.date
where 1=1
;

