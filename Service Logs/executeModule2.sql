
select distinct client_name
from dev.harvesting.modules_v0
sample (10)
where 1=1
limit 5
;

with z as (
select  
    regexp_replace(metadata_client,'-qa','') as client,
    (case when metadata_client regexp '^.*-qa.*$' then concat('qa-', metadata_env) else metadata_env end) as env,
    target_name,
    target_port,
    target_status_code,
    request,
    received_bytes,
    (case
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    when request_uri is NULL and user_agent regexp '^.*Datadog.*$' then 'datadog'
    when request_uri is NULL then 'NULL'
    when request_uri = '/ping' then 'ping'
    when request_uri = '/logout' then 'logout'
    when request_uri regexp '^/auth/.*$' then 'authorization'
    when request_uri = '/fbu/uapi/pdfbar/combine' and request_method = 'POST' then 'Combine PDFs'
    when request_uri = '/fbu/uapi/image/concatenate' and request_method = 'POST' then 'Concatenate images'
    when request_uri = '/fbu/uapi/image/convert' and request_method = 'POST' then 'Convert and image or document'
    when request_uri = '/fbu/uapi/transformer/xml2js' and request_method = 'POST' then 'Convert XML to JSON'
    when request_uri = '/fbu/uapi/system/hashString' and request_method = 'POST' then 'Create has of passed string'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create module submissions from data returned by paper ingestion service'
    when request_uri regexp '^/fbu/uapi/paperIngestion/jobs/[0-9a-z]*/status$' and request_method = 'GET' then 'Get Status'
    when request_uri = '/fbu/uapi/paperIngestion/jobs' and request_method = 'POST' then 'Post an image to an external paper ingestion OCR service'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Module'
    when request_uri regexp '^/fbu/uapi/forms/[0-9a-z]*/execute$' and request_method = 'PUT' then 'Execute Form'
    when request_uri = '/fbu/uapi/transformer' and request_method = 'POST' then 'Execute Transform with Input Data'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/api/.*$' then concat('Execute via Proxy - ',request_method)
    when request_uri = '/fbu/uapi/fileUtils/json2csv' and request_method = 'POST' then 'Generate a CSV file'
    when request_uri = '/fbu/uapi/fileUtils/json2zip' and request_method = 'POST' then 'Generate a zip file'
    when request_uri = '/fbu/uapi/contentpdf' and request_method = 'POST' then 'Generate PDF from HTML'
    when request_uri = '/fbu/uapi/pdfbar' and request_method = 'POST' then 'Generate PDF from Template (PDF Bar)'
    when request_uri = '/fbu/uapi/referstring' and request_method = 'POST' then 'Generates an encrypted referstring for authentication'
    when request_uri = '/fbu/uapi/configurationAnalysis' and request_method = 'GET' then 'Get configuration analysis results'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/distinct$' and request_method = 'GET' then 'Get Distinct Values from Reference Data'
    when request_uri regexp '^/fbu/uapi/submissions/merge/[0-9a-z]*$' and request_method = 'GET' then 'Get Merged Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method = 'GET' then 'Get Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Module Submission Revisions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'GET' then 'Get Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'DELETE' then 'Delete Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'DELETE' then 'Delete Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method = 'PUT' then 'Update Module Submission'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'PUT' then 'Update Module Submissions'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*/submissions\\?.*$' and request_method = 'POST' then 'Create Module Submission(s)'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/[0-9a-z\-]*$' and request_method= 'POST' then 'Create Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'GET' then 'Get Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/revisions/[0-9a-z]*$' and request_method = 'GET' then 'Get Worfklow Submission Revisions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'GET' then 'Get Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'DELETE' then 'Delete Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions\\?.*$' and request_method= 'DELETE' then 'Delete Multiple Workflow Submissions'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*/restore$' and request_method = 'POST' then 'Restore a Deleted Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/submissions/[0-9a-z]*\\?.*$' and request_method= 'PUT' then 'Update Workflow Submission'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri regexp '^/fbu/uapi/query/[0-9a-z:]*/all\\?.*$' and request_method = 'GET' then 'Get Rows from Reference Data'
    when request_uri = '/fbu/uapi/logs/services' and request_method = 'GET' then 'Get Service Logs'
    when request_uri regexp '^/fbu/uapi/logs/services\\?.*$' and request_method = 'GET' then 'Get Service Logs'
    when request_uri = '/fbu/uapi/fileUtils/decrypt/gpg' and request_method = 'PUT' then 'GPG Decrypt a File'
    when request_uri = '/fbu/uapi/fileUtils/encrypt/gpg' and request_method = 'PUT' then 'GPG Encrypt a File'
    when request_uri regexp '^/fbu/uapi/workflow/[0-9a-z]*/handoff$' and request_method = 'PUT' then 'Handoff Submission'
    when request_uri = '/fbu/uapi/system/getIncrementalCounter' and request_method = 'GET' then 'Incremental Counter'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart$' and request_method = 'GET' then 'List of timer start nodes and statuses'
    when request_uri = '/fbu/uapi/system/getSubmissions' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/system/getSubmissions\\?.*$' and request_method = 'GET' then 'List Submissions for Dashboard'
    when request_uri regexp '^/fbu/uapi/workflow-execute/[0-9a-z]*/resume/[0-9a-z]*/submission/[0-9a-z]*$' and request_method = 'GET' then 'Resume Workflow'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/run-once$' and request_method = 'POST' then 'Run a Timer Start Node Once (Test Run)'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/start$' and request_method = 'POST' then 'Start a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/workflows/[0-9a-z]*/timerstart/[0-9a-z]*/stop$' and request_method = 'POST' then 'Stop a Timer Start Node'
    when request_uri regexp '^/fbu/uapi/dataCollections/[0-9a-z]*/import$' and request_method = 'POST' then 'Update Data Collection via Import'
    when request_uri = '/fbu/uapi/excelFill' and request_method = 'POST' then 'Upload Excel Template'
    when request_uri regexp '^.*/forms/.*$' then 'form'
    when request_uri regexp '^.*/form/.*$' then 'form'
    when request_uri regexp '^.*/transformer.*$' then 'transform'
    when request_uri regexp '^.*/modules/.*/submissions$' then 'moduleSubmissions'
    when request_uri regexp '^.*/workflows/.*/submissions$' then 'workflowSubmissions'
    when request_uri regexp '^.*/promote/.*$' then 'promotion'
    when request_uri regexp '^.*/modules.*$' then 'module'
    when request_uri regexp '^.*/workflow.*$' then 'workflow'
    when request_uri regexp '^/fbu/uapi/query/.*$' then 'query'
    when request_uri regexp '^/fbu/uapi/services/.*$' then 'services'
    when request_uri = '/fbu/uapi/tracker' then 'tracker'
    when request_uri regexp '^/fbu/files/pdf/.*$' then 'pdf'
    when request_uri regexp '^/fbu/files/pdf/combined-pdf.*$' then 'combined-pdf'
    when user_agent regexp '^.*Datadog.*$' then 'datadog'
    when user_agent regexp '^python.*$' then 'python'
    when user_agent regexp '^.*curl.*$' then 'curl'
    else 'Other'
    end) as request_type,
    (case when request_uri regexp '^.*\\?.*$' then 'True' else 'False' end) as Conditional,
    (case 
        when request_uri regexp '^/fbu/uapi/modules/[0-9a-z]*.*$' then regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/modules/[0-9a-z]*'),'/fbu/uapi/modules/','') 
        else regexp_replace(regexp_substr(request_uri,'^.*moduleId=[0-9a-z]*'),'^.*moduleId=','') 
    end) as moduleId,
    regexp_replace(regexp_substr(request_uri,'^/fbu/uapi/workflow?/[0-9a-z-]*'),'/fbu/uapi/workflow?/','') as workflow,
    sent_bytes,
    request_method,
    user_agent,
    request_uri,
    (case 
     when target_processing_time < 0 then 0
     else target_processing_time 
     end) as processing_time,
    to_timestamp(concat(metadata_date,  ' ', to_char(hour(time)), ':', to_char(truncate(minute(time)/2)*2)), 'YYYY-MM-DD HH24:MI') as date,
    time
from prod.harvesting.V_AWS_ELB_LOGS
where 1=1
    and metadata_client in (%(client)s)
    and metadata_env in (%(env)s)
    --and time >= dateadd(day,-30,current_date())
    and time between between %(startDate)s and %(endDate)s
    and request_method in ('GET','POST','PUT','DELETE','PATCH')
), a as (
select 
    client as client_name,
    env,
    time,
    target_status_code as responsecode,
    request_type,
    request,
    request_uri,
    moduleid,
    user_agent,
    conditional,
    received_bytes,
    sent_bytes,
    request_method,
    processing_time as service_response_time,
    date
from z
where 1=1
    and request_type = 'Execute Module'
), b as (
select 
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type,
    count(1)  as component_count
from 
    dev.harvesting.archives_definitions_components_v0 dc,
    dev.harvesting.archives_definitions_v0 d
where 1=1
    and dc.fk_archive = d.pk_archive
    and d.client_name in (select distinct client_name from a)
    and d.env in (select distinct env from a)
group by
    d.client_name,
    d.env,
    d.formid,
    d.created_date,
    d.modified_date,
    dc.type
), c as (
select 
    a.client_name,
    a.env,
    a.moduleid,
    a.created_date,
    b.formid,
    a.service_response_time,
    a.responsecode,
    b.created_date as archive_date,
    b.type,
    b.component_count
from a, b
where 1=1
    and a.client_name = b.client_name
    and a.env = b.env
    and a.form_id = b.formid
    and a.created_date >= b.created_date
), d as (
select 
    client_name,
    env,
    id,
    created_date,
    formid,
    service_response_time,
    servicename,
    componentkey,
    method,
    responsecode,
    type,
    component_count,
    archive_date,
    max(archive_date) over (partition by id) as max_archive_date
from c
)
select 
    client_name,
    env,
    id,
    created_date,
    formid,
    service_response_time,
    servicename,
    componentkey,
    method,
    responsecode,
    type,
    component_count
from d
where 1=1
    and archive_date = max_archive_date
order by
    client_name,
    env,
    formid,
    created_date
;
